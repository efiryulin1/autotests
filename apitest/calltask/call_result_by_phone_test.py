
# Тест проверяет получение информации о звонках по номеру телефона для входящих проектов.

import requests
import pytest
from apitest.calltask.variables_calltask import PhoneNumber, CallsNumber


# тест получения инфо о звонках (количество звонков - рандомное число от 1 до 12)
@pytest.mark.positive
@pytest.mark.api
def test_result_by_phone_rand(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3,
            'number_of_calls': CallsNumber.rand_number}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, проверяем, что длинна массива равно количесву звонков, указаному в запросе
    # в каждом элементе массива проверяем номер телефона
    assert res.status_code == 200
    assert len(res_json) == CallsNumber.rand_number, "длинна массива отличается от number_of_calls"
    for call in range(len(res_json)):
        assert res_json[call]["data"]["phone_number"] == str(PhoneNumber.phone3), "другой ответ"


# тест получения инфо о звонках (кол-во звонков - 1). Используются данные для параметризации теста data_result_by_phone
@pytest.mark.positive
@pytest.mark.api
def test_result_by_phone_1(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3,
            'number_of_calls': CallsNumber.number_of_calls}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, длину массива (должна быть равна кол-ву хвонков, указанному в заросе
    # или одному, если ничего не указано, и номер телефона в ответе
    assert res.status_code == 200
    assert len(res_json) == CallsNumber.number_of_calls, "длинна массива отличается от number_of_calls"
    assert res_json[0]["data"]["phone_number"] == str(PhoneNumber.phone3), "другой ответ"


# тест получения инфо о звонка без указания параметра number_of_calls
@pytest.mark.positive
@pytest.mark.api
def test_result_by_phone_wo_number(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200
    assert res_json[0]["data"]["phone_number"] == str(PhoneNumber.phone3), "другой ответ"


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key(env_url_result_by_phone_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_api_key', "другой ответ"


# Тест проверяет валидацию параметра project_id
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id(env_url_result_by_phone_in, env_api_key_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': '', 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_project_id', "другой ответ"


# Тест проверяет валидацию сочетания пар api_key и project_id
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_url_result_by_phone_in, env_project_id_in, env_api_key):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# Тест проверяет валидацию сочетания пар api_key и project_id
@pytest.mark.negative
@pytest.mark.api
def test_invalid_project_id(env_url_result_by_phone_in, env_api_key_in, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id, 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# Тест проверяет валидацию поля api_key
@pytest.mark.negative
@pytest.mark.api
def test_wrong_api_key(env_url_result_by_phone_in, env_project_id_in):

    # готовим данные, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'api_key', 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# Тест проверяет валидацию параметра phone (пустой)
@pytest.mark.negative
@pytest.mark.api
def test_empty_phone(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': ''}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_phone', "другой ответ"


# Тест проверяет валидацию параметра phone (неправильный номер)
@pytest.mark.negative
@pytest.mark.api
def test_wrong_phone(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': '70000000000'}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == 'no_info_about_this_call', "другой ответ"


# Тест проверяет валидацию параметра phone (вместо цифр используются буквы)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_phone(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': 'phone'}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'regex_phone', "другой ответ"


# Тест проверяет валидацию параметра number_of_calls.
@pytest.mark.negative
@pytest.mark.api
def test_numeric_number_of_calls(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3,
            'number_of_calls': 'number_of_calls'}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'numeric_number_of_calls', "другой ответ"


# тест валидации параметра number_of_calls (пустое)
@pytest.mark.negative
@pytest.mark.api
def test_empty_number_of_calls(env_url_result_by_phone_in, env_api_key_in, env_project_id_in):

    # готовим данные, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_in, 'project_id': env_project_id_in, 'phone': PhoneNumber.phone3,
            'number_of_calls': ''}
    res = requests.get(env_url_result_by_phone_in, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'numeric_number_of_calls', "другой ответ"
