
# Тест проверяет массовое получение информации о звонках

import requests
import pytest
import json
import time
from apitest.calltask.variables_calltask import PhoneNumber


# тест получение информации о звонках по call_task_ids
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_call_task_ids(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_1,
                                 env_call_task_bulk_3, env_call_task_bulk_5, env_call_task_bulk_7,
                                 env_call_task_bulk_9, env_date_call_task_bulk_1, env_date_call_task_bulk_3,
                                 env_date_call_task_bulk_5, env_date_call_task_bulk_7, env_date_call_task_bulk_9):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {"api_key": env_api_key, "project_id": env_project_id,
            "call_task_ids[]": [env_call_task_bulk_1, env_call_task_bulk_3, env_call_task_bulk_5, env_call_task_bulk_7,
                                env_call_task_bulk_9]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_1)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_1)]["init_time"] == env_date_call_task_bulk_1, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_3)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_3)]["init_time"] == env_date_call_task_bulk_3, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_5)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_5)]["init_time"] == env_date_call_task_bulk_5, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_7)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_7)]["init_time"] == env_date_call_task_bulk_7, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_9)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_9)]["init_time"] == env_date_call_task_bulk_9, "wrong answer"


# тест получение информации о звонках по bulk_id
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_bulk_id(env_url_result_bulk, env_api_key, env_project_id, env_bulk_2, env_call_task_bulk_3,
                           env_call_task_bulk_4, env_date_call_task_bulk_3, env_date_call_task_bulk_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'bulk_id': env_bulk_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_3)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_3)]["init_time"] == env_date_call_task_bulk_3, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_4)]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_4)]["init_time"] == env_date_call_task_bulk_4, "wrong answer"


# тест получение информации о звонках по tag (одинаковый тэг использовался на разных проектах)
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_overall_tag(env_url_result_bulk, env_api_key, env_project_id, env_tag_4, env_call_task_bulk_9,
                               env_call_task_bulk_10, env_call_task_bulk_13, env_date_call_task_bulk_9,
                               env_date_call_task_bulk_10, env_date_call_task_bulk_13):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_9)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_9)]["init_time"] == env_date_call_task_bulk_9, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_10)]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_10)]["init_time"] == env_date_call_task_bulk_10, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_13)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_13)]["init_time"] == env_date_call_task_bulk_13, "wrong answer"


# тест получение информации о звонках по tag, call_task_ids и bulk_id. В ответе должны получить информацию только о тех
# звонках, которые подходят под все критерии
@pytest.mark.positive
@pytest.mark.smoke
@pytest.mark.api
def test_result_by_bulk_call_task_tag(env_url_result_bulk, env_api_key, env_project_id, env_tag_4, env_bulk_5,
                                      env_call_task_bulk_1, env_call_task_bulk_3, env_call_task_bulk_5,
                                      env_call_task_bulk_9, env_date_call_task_bulk_9):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": env_bulk_5,
            "call_task_ids[]": [env_call_task_bulk_1, env_call_task_bulk_3, env_call_task_bulk_5,
                                env_call_task_bulk_9, env_date_call_task_bulk_9], 'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_9)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_9)]["init_time"] == env_date_call_task_bulk_9, "wrong answer"


# тест проверяет получение инфо о звонке. Используются разные сочетание call_task_id и bulk_id
# в ответе должен быть только один звонок, т.к. только он подходит по пару call_task_id и bulk_id
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_bulk_call_task(env_url_result_bulk, env_api_key, env_project_id, env_bulk_2, env_call_task_bulk_1,
                                  env_call_task_bulk_3, env_call_task_bulk_5, env_call_task_bulk_12, env_date_call_task_bulk_3):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": env_bulk_2,
            "call_task_ids[]": [env_call_task_bulk_1, env_call_task_bulk_3, env_call_task_bulk_5, env_call_task_bulk_12]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_3)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_3)]["init_time"] == env_date_call_task_bulk_3, "wrong answer"


# тест использует проверяет получение ответа при сочетании параметров tag и bulk_id
# в ответе должен быть только один звонок, т.к. только он подходит по пару tag и bulk_id
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_bulk_tag(env_url_result_bulk, env_api_key, env_project_id, env_bulk_2, env_tag_1,
                            env_call_task_bulk_3, env_call_task_bulk_4, env_date_call_task_bulk_3, env_date_call_task_bulk_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": env_bulk_2, "tag": env_tag_1}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_3)]["phone_number"] == str(PhoneNumber.phone2), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_3)]["init_time"] == env_date_call_task_bulk_3, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_4)]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_4)]["init_time"] == env_date_call_task_bulk_4, "wrong answer"


# тест использует проверяет получение ответа при сочетании параметров tag и call_task_ids
# в ответе должен быть только один звонок, т.к. только он подходит по пару tag и call_task_ids
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_call_task_tag(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_11,
                                 env_call_task_bulk_12, env_call_task_bulk_13, env_tag_2, env_date_call_task_bulk_12):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            "call_task_ids[]": [env_call_task_bulk_11, env_call_task_bulk_12, env_call_task_bulk_13], 'tag': env_tag_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_12)]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_12)]["init_time"] == env_date_call_task_bulk_12, "wrong answer"


# тест проверяет инфомацию о звонках. Один звонок был выполнен, два - нет.
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_part_result_by_call_task_ids(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_14,
                                      env_call_task_bulk_15, env_call_task_bulk_16, env_date_call_task_bulk_14):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {"api_key": env_api_key, "project_id": env_project_id,
            "call_task_ids[]": [env_call_task_bulk_14, env_call_task_bulk_15, env_call_task_bulk_16]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    # в двух зонках проверяем сообщение об ощибке (для звонков использовались параметры max_datetime_to_call и
    # min_datetime_to_call
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"][str(env_call_task_bulk_16)]["errors"]["title"] == "time_is_up", "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_14)]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_14)]["init_time"] == env_date_call_task_bulk_14, "wrong answer"
    assert res_json["data"][str(env_call_task_bulk_15)]["errors"]["title"] == "not_called_yet", "wrong answer"


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key_result_bulk(env_url_result_bulk, env_project_id, env_bulk_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id, 'bulk_id': env_bulk_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "required_api_key", "другой ответ"


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key_result_bulk(env_url_result_bulk, env_project_id, env_bulk_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'invalid_api_key', 'project_id': env_project_id, 'bulk_id': env_bulk_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "unknown_api_key", "другой ответ"


# тест валидации параметра project_id.
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id_result_bulk(env_url_result_bulk, env_api_key, env_bulk_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': '', 'bulk_id': env_bulk_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "required_project_id", "другой ответ"


# тест валидации параметра project_id и api_key
@pytest.mark.negative
@pytest.mark.api
def test_wrong_project_id(env_url_result_bulk, env_api_key, env_project_id_fail, env_tag_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id_fail, 'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "project_id_fail", "другой ответ"


# тест валидации параметра project_id и api_key
@pytest.mark.negative
@pytest.mark.api
def test_wrong_api_key(env_url_result_bulk, env_api_key_fail, env_project_id, env_tag_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "project_id_fail", "другой ответ"


# тест валидации параметров call_task_ids
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_1(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "call_task_ids[]": [0]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_ids
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_2(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "call_task_ids[]": ''}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_ids
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_3(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "call_task_ids[]": 'call_task_ids'}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест проверяет валидацию параметра call_task_ids (должен быть массивом)
@pytest.mark.negative
@pytest.mark.api
def test_not_an_array(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {"api_key": env_api_key, "project_id": env_project_id, "call_task_ids": [000000]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "array_call_task_ids", "другой ответ"


# тест валидации параметров call_task_ids
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_4(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_fail_1):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "call_task_ids[]": [env_call_task_bulk_fail_1]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров bulk_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_bulk_id_1(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": 0}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров bulk_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_bulk_id_2(env_url_result_bulk, env_api_key, env_project_id, env_bulk_fail_1):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": env_bulk_fail_1}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_tag_1(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "tag": 123}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_tag_2(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "tag": '123 тест'}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_tag_3(env_url_result_bulk, env_api_key, env_project_id, env_tag_3):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "tag": env_tag_3}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров tag, bulk_id, call_task_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_tag_bulk_id_call_task_id_1(env_url_result_bulk, env_api_key, env_project_id, env_tag_2, env_bulk_1,
                                            env_call_task_bulk_3, env_call_task_bulk_11):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "tag": env_tag_2, 'bulk_id': env_bulk_1,
            'call_task_ids[]': [env_call_task_bulk_3, env_call_task_bulk_11]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров tag, bulk_id, call_task_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_tag_bulk_id_call_task_id_2(env_url_result_bulk, env_api_key, env_project_id, env_tag_3, env_bulk_fail_1,
                                            env_call_task_bulk_fail_1):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "tag": env_tag_3, 'bulk_id': env_bulk_fail_1,
            'call_task_ids[]': [env_call_task_bulk_fail_1]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров bulk_id, call_task_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_bulk_id_call_task_id_1(env_url_result_bulk, env_api_key, env_project_id, env_bulk_1,
                                        env_call_task_bulk_3):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'bulk_id': env_bulk_1,
            'call_task_ids[]': [env_call_task_bulk_3]}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров bulk_id, tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_bulk_id_tag_1(env_url_result_bulk, env_api_key, env_project_id, env_bulk_1, env_tag_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'bulk_id': env_bulk_1, 'tag': env_tag_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_id, tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_tag_1(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_1, env_tag_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_ids[]': env_call_task_bulk_1,
            'tag': env_tag_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_id, tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_tag_2(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_11, env_tag_2):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_ids[]': env_call_task_bulk_11,
            'tag': env_tag_2}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_id, tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_tag_3(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_6, env_tag_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_ids[]': env_call_task_bulk_6,
            'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_id, tag
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_tag_4(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_7,
                                    env_call_task_bulk_8, env_tag_4):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'call_task_ids[]': [env_call_task_bulk_7, env_call_task_bulk_8], 'tag': env_tag_4}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметров call_task_id, bulk_id
@pytest.mark.negative
@pytest.mark.api
def test_unknown_call_task_id_bulk_id_1(env_url_result_bulk, env_api_key, env_project_id, env_call_task_bulk_11,
                                        env_bulk_1):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_ids[]': env_call_task_bulk_11,
            'bulk_id': env_bulk_1}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест проверяет валидацию параметра (его отсутствие) по которму мы ищем звонок
@pytest.mark.negative
@pytest.mark.api
def test_empty_id(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {"api_key": env_api_key, "project_id": env_project_id}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown", "другой ответ"


# тест проверяет валидацию параметра bulk_id.
@pytest.mark.negative
@pytest.mark.api
def test_empty_bulk_id(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": ''}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "integer_bulk_id", "другой ответ"


# тест проверяет валидацию параметра bulk_id.
@pytest.mark.negative
@pytest.mark.api
def test_invalid_bulk_id(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": 'integer_bulk_id'}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "integer_bulk_id", "другой ответ"


# тест проверяет валидацию параметра bulk_id.
@pytest.mark.negative
@pytest.mark.api
def test_wrong_bulk_id(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, "bulk_id": 46513213454564646566132}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "integer_bulk_id", "другой ответ"


# тест проверяет валидацию параметра tag
@pytest.mark.negative
@pytest.mark.api
def test_empty_tag(env_url_result_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {"api_key": env_api_key, "project_id": env_project_id, "tag": ''}
    res = requests.post(env_url_result_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "string_tag", "другой ответ"


@pytest.mark.usefixtures("create_new_call_task_bulk")
class TestNewCallCheckBulk:

    # тест проверяет получение инфомации о новом звонке по bulk_id (звонок запускается до запуска теста и передает параметры для
    # проверки в тест). Используется фикстура create_new_call_task_bulk из файла conftest.py
    @pytest.mark.positive
    @pytest.mark.api
    @pytest.mark.smoke
    def test_new_call_check_by_bulk(self, create_new_call_task_bulk, env_url_result_bulk, env_api_key, env_project_id):

        # принимаем параметры res_call_json, rand_pass1, rand_pass2 из фикстуры
        res_call_json, rand_pass1, rand_pass2 = create_new_call_task_bulk
        res_bulk_id = res_call_json["bulk_id"]
        res_call_id_1 = res_call_json["0"]["data"]["call_task_id"]
        res_call_id_2 = res_call_json["1"]["data"]["call_task_id"]

        # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
        # запускаем цикл, который каждые 2 секунды стучится в апи и проверяет подтянулась инфа по звонку или нет
        data = {'api_key': env_api_key, 'project_id': env_project_id, 'bulk_id': res_bulk_id}
        for i in range(180):
            res = requests.post(env_url_result_bulk, data=data)
            res_json = res.json()
            if 'not_called_yet' in json.dumps(res_json["data"][str(res_call_id_2)]):
                time.sleep(3)
            else:
                break

        # когда инфа подтянулась проверяем ответ: код ответа, время ответа. В ответе проверяем статус звонка и
        # код (по сути пароль), который передали в запросе.
        assert res.status_code == 200, "status code is not 200"
        assert res_json["data"][str(res_call_id_1)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_1)]['data']['number'] == rand_pass1, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['data']['number'] == rand_pass2, "wrong answer"

    # тест проверяет получение инфомации о новом звонке по call_task_id (звонок запускается до запуска теста и
    # передает параметры для проверки в тест). Используется фикстура create_new_call_task_bulk из файла conftest.py
    @pytest.mark.positive
    @pytest.mark.api
    @pytest.mark.smoke
    def test_new_call_check_by_call_task(self, create_new_call_task_bulk, env_url_result_bulk, env_api_key, env_project_id):

        # принимаем параметры res_call_json, rand_pass1, rand_pass2 из фикстуры
        res_call_json, rand_pass1, rand_pass2 = create_new_call_task_bulk
        res_call_id_1 = res_call_json["0"]["data"]["call_task_id"]
        res_call_id_2 = res_call_json["1"]["data"]["call_task_id"]

        # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
        # запускаем цикл, который каждые 2 секунды стучится в апи и проверяет подтянулась инфа по звонку или нет
        data = {'api_key': env_api_key, 'project_id': env_project_id,
                'call_task_ids[]': [res_call_id_1, res_call_id_2]}
        for i in range(180):
            res = requests.post(env_url_result_bulk, data=data)
            res_json = res.json()
            if 'not_called_yet' in json.dumps(res_json["data"][str(res_call_id_2)]):
                time.sleep(3)
            else:
                break

        # когда инфа подтянулась проверяем ответ: код ответа, время ответа. В ответе проверяем статус звонка и
        # код (по сути пароль), который передали в запросе.
        assert res.status_code == 200, "status code is not 200"
        assert res_json["data"][str(res_call_id_1)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_1)]['data']['number'] == rand_pass1, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['data']['number'] == rand_pass2, "wrong answer"

    # тест проверяет получение инфомации о новом звонке по tag (звонок запускается до запуска теста и
    # передает параметры для проверки в тест). Используется фикстура create_new_call_task_bulk из файла conftest.py
    @pytest.mark.positive
    @pytest.mark.api
    @pytest.mark.smoke
    def test_new_call_check_by_tag(self, create_new_call_task_bulk, env_url_result_bulk, env_api_key, env_project_id, env_tag_5):

        # принимаем параметры res_call_json, rand_pass1, rand_pass2 из фикстуры
        res_call_json, rand_pass1, rand_pass2 = create_new_call_task_bulk
        res_call_id_1 = res_call_json["0"]["data"]["call_task_id"]
        res_call_id_2 = res_call_json["1"]["data"]["call_task_id"]

        # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
        # запускаем цикл, который каждые 2 секунды стучится в апи и проверяет подтянулась инфа по звонку или нет
        data = {'api_key': env_api_key, 'project_id': env_project_id, 'tag': env_tag_5}
        for i in range(180):
            res = requests.post(env_url_result_bulk, data=data)
            res_json = res.json()
            if 'not_called_yet' in json.dumps(res_json["data"][str(res_call_id_2)]):
                time.sleep(3)
            else:
                break

        # когда инфа подтянулась проверяем ответ: код ответа, время ответа. В ответе проверяем статус звонка и
        # код (по сути пароль), который передали в запросе.
        assert res.status_code == 200, "status code is not 200"
        assert res_json["data"][str(res_call_id_1)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_1)]['data']['number'] == rand_pass1, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['status'] == 22, "wrong answer"
        assert res_json["data"][str(res_call_id_2)]['data']['number'] == rand_pass2, "wrong answer"
