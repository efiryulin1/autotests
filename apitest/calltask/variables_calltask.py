
# Данные для запроса, разделены на классы. Используется в тестах звонков

import random
import json


class URL:
    # DEV
    call_result_by_phone_url_dev = 'https://godev.robotmia.ru/api/call/result/by-phone'
    calltask_result_by_phone_url_dev = 'https://godev.robotmia.ru/api/calltask/result-by-phone'
    calltask_result_url_dev = 'https://godev.robotmia.ru/api/calltask/result'
    calltask_create_url_dev = 'https://godev.robotmia.ru/api/calltask'
    calltask_create_bulk_url_dev = 'https://godev.robotmia.ru/api/calltask/bulk'
    calltask_result_bulk_url_dev = 'https://godev.robotmia.ru/api/calltask/result-bulk'

    # PROD
    call_result_by_phone_url_prod = 'https://go.robotmia.ru/api/call/result/by-phone'
    calltask_result_by_phone_url_prod = 'https://go.robotmia.ru/api/calltask/result-by-phone'
    calltask_result_url_prod = 'https://go.robotmia.ru/api/calltask/result'
    calltask_create_url_prod = 'https://go.robotmia.ru/api/calltask'
    calltask_create_bulk_url_prod = 'https://go.robotmia.ru/api/calltask/bulk'
    calltask_result_bulk_url_prod = 'https://go.robotmia.ru/api/calltask/result-bulk'


class ApiKey:
    # DEV
    api_key_project_1_dev = 'f3506b12-1ac6-4847-a3c8-f3ae29d152be'
    api_key_project_6 = '7914a1cc-25b7-47b5-bb75-34e44b6fff74'
    api_key_project_13 = '6edc4d03-fb0f-42e5-8d54-2894b5a3d3f8'
    api_key_10k_dev = 'badbd5b7-65d1-47fb-a75b-a84c294cc780'  # ключ для 247 запуска на 13 проекте

    # PROD
    api_key_project_1_prod = '54b0587e-55e5-40f6-a5d8-dbe25c7c7eab'  # для входящих звонков
    api_key_project_4 = '92fe7793-0f22-4db6-bd62-1fbe6f903f7e'  # ключ для 6806 запуска на 4 проекте
    api_key_project_172 = 'b9bfbf09-9f07-4057-bca5-18a20ab98491'  # ключ для 7933 запуска на 172 проекте
    api_key_10k_prod = '564bb91e-65bb-4453-b93f-ec667b837a7a'  # ключ для 7934 запуска на 172 проекте


class ProjectId:
    project_id_1 = '1'

    # DEV
    # project_id_1_dev = '1'
    project_id_6 = '6'
    project_id_13 = '13'

    # PROD
    # project_id_1_prod = '1'
    project_id_4 = '4'
    project_id_172 = '172'


class PhoneNumber:
    phone1 = '79231265383'
    phone2 = '79231265336'
    phone3 = '79529252021'


class CallTask:
    wrong_call_task_id = '950452'

    #______________________ DEV _________________________#
    call_task_1_dev = '955640'

    bulk_id_1_pr_13 = '13221118090606207'  # тэг 1
    call_task_id_1_pr_13 = '1026109'
    call_task_id_2_pr_13 = '1026110'

    bulk_id_2_pr_13 = '13221118090705269'  # тэг 1
    call_task_id_3_pr_13 = '1026111'
    call_task_id_4_pr_13 = '1026112'

    bulk_id_3_pr_13 = '13221118094926169'  # тэг 2
    call_task_id_5_pr_13 = '1026113'
    call_task_id_6_pr_13 = '1026114'

    bulk_id_4_pr_13 = '13221118100935864'  # без тэга
    call_task_id_7_pr_13 = '1026115'
    call_task_id_8_pr_13 = '1026116'

    bulk_id_5_pr_13 = '1322111810245168'  # тэг 4
    call_task_id_9_pr_13 = '1026121'
    call_task_id_10_pr_13 = '1026122'

    call_task_id_11_pr_13 = '1026123'  # с тэгом 1
    call_task_id_12_pr_13 = '1026124'  # с тэгом 2
    call_task_id_13_pr_13 = '1026125'  # с тэгом 4
    call_task_id_14_pr_13 = '1026126'  # без тэга
    call_task_id_15_pr_13 = '1026146'  # еще не звонили
    call_task_id_16_pr_13 = '955657'  # просроченный звонок

    bulk_id_1_pr_1 = '1221118101606944'  # тэг 1
    call_task_id_1_pr_1 = '1026117'
    call_task_id_2_pr_1 = '1026118'

    bulk_id_2_pr_1 = '1221118102008376'  # общий тэг
    call_task_id_3_pr_1 = '1026119'
    call_task_id_4_pr_1 = '1026120'

    call_task_id_5_pr_1 = '1026128'  # с тэгом 3
    call_task_id_6_pr_1 = '1026127'  # с тэгом 4

    # ______________________ PROD _________________________#
    call_task_1_prod = '9208516'

    bulk_id_1_pr_172 = '172230113085237902'  # тэг 1
    call_task_id_1_pr_172 = '9307276'
    call_task_id_2_pr_172 = '9307277'

    bulk_id_2_pr_172 = '172230113085258110'  # тэг 1
    call_task_id_3_pr_172 = '9307286'
    call_task_id_4_pr_172 = '9307287'

    bulk_id_3_pr_172 = '172230113085414416'  # тэг 2
    call_task_id_5_pr_172 = '9307329'
    call_task_id_6_pr_172 = '9307330'

    bulk_id_4_pr_172 = '172230113085536535'  # без тэга
    call_task_id_7_pr_172 = '9307375'
    call_task_id_8_pr_172 = '9307376'

    bulk_id_5_pr_172 = '172230113085627893'  # тэг 4
    call_task_id_9_pr_172 = '9307448'
    call_task_id_10_pr_172 = '9307449'

    call_task_id_11_pr_172 = '9308410'  # с тэгом 1
    call_task_id_12_pr_172 = '9308429'  # с тэгом 2
    call_task_id_13_pr_172 = '9308459'  # с тэгом 4
    call_task_id_14_pr_172 = '9308472'  # без тэга
    call_task_id_15_pr_172 = '9308711'  # еще не звонили
    call_task_id_16_pr_172 = '9308718'  # просроченный звонок

    bulk_id_1_pr_4 = '4230113091602749'  # тэг 1
    call_task_id_1_pr_4 = '9308787'
    call_task_id_2_pr_4 = '9308788'

    bulk_id_2_pr_4 = '4230113091628246'  # общий тэг
    call_task_id_3_pr_4 = '9308835'
    call_task_id_4_pr_4 = '9308836'

    call_task_id_5_pr_4 = '9309003'  # с тэгом 3
    call_task_id_6_pr_4 = '9309049'  # с тэгом 4


class DateToCall:
    min_datetime_to_call = '4000-12-31 23:59:59'
    max_datetime_to_call = '2022-01-01 12:00:00'
    invalid_datetime = '2000-32-50 34:45:99'

    # DEV
    date_call_task_1_dev = '2022-11-11 07:26:25'
    date_call_task_bulk_1_dev = '2022-11-18 09:06:28'
    date_call_task_bulk_3_dev = '2022-11-18 09:07:29'
    date_call_task_bulk_4_dev = '2022-11-18 09:07:44'
    date_call_task_bulk_5_dev = '2022-11-18 09:50:28'
    date_call_task_bulk_7_dev = '2022-11-18 10:10:31'
    date_call_task_bulk_9_dev = '2022-11-18 10:25:29'
    date_call_task_bulk_10_dev = '2022-11-18 10:25:44'
    date_call_task_bulk_11_dev = '2022-11-18 10:25:44'
    date_call_task_bulk_12_dev = '2022-11-18 10:41:31'
    date_call_task_bulk_13_dev = '2022-11-18 10:41:46'
    date_call_task_bulk_14_dev = '2022-11-18 10:42:01'

    # PROD
    date_call_task_1_prod = '2023-01-11 08:20:18'
    date_call_task_bulk_1_prod = '2023-01-13 08:53:14'
    date_call_task_bulk_3_prod = '2023-01-13 08:54:12'
    date_call_task_bulk_4_prod = '2023-01-13 08:54:14'
    date_call_task_bulk_5_prod = '2023-01-13 08:55:11'
    date_call_task_bulk_7_prod = '2023-01-13 08:56:13'
    date_call_task_bulk_9_prod = '2023-01-13 08:57:14'
    date_call_task_bulk_10_prod = '2023-01-13 08:57:16'
    date_call_task_bulk_11_prod = '2023-01-13 08:57:16'
    date_call_task_bulk_12_prod = '2023-01-13 09:09:12'
    date_call_task_bulk_13_prod = '2023-01-13 09:10:12'
    date_call_task_bulk_14_prod = '2023-01-13 09:11:12'


class CallsNumber:
    number_of_calls = 1
    max_number_of_calls = 12  # кол-во входящих звонков на номер phone на 6 проекте
    rand_number = random.randint(number_of_calls, max_number_of_calls)


class Tag:
    tag_create = 'тестовый запуск calltask create'
    tag_create_bulk = 'тестовый запуск calltask create bulk'

    # DEV
    tag_1_pr_13 = 'тэг на 13 проекте №1'
    tag_2_pr_13 = 'тэг на 13 проекте №2'
    tag_3_pr_1 = 'тэг на 1 проекте №1'
    tag_4_pr_13_pr_1 = 'общий тэг на 1 и 13 проектах'
    tag_5_pr_13 = 'тэг на 13 проекте №3'

    # PROD
    tag_1_pr_172 = 'тэг на 172 проекте №1'
    tag_2_pr_172 = 'тэг на 172 проекте №2'
    tag_3_pr_4 = 'тэг на 4 проекте №1'
    tag_4_pr_172_pr_4 = 'общий тэг на 4 и 172 проектах'
    tag_5_pr_172 = 'тэг на 172 проекте №3'


class SipHeaders:
    sip_headers = json.dumps({"trunk_name": "trunk_79239123344"})
    sip_headers_bulk1 = json.dumps({"trunk_name": "trunk_79239123311"})
    sip_headers_bulk2 = json.dumps({"trunk_name": "trunk_79239123322"})
