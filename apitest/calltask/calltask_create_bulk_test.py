
# Тест проверяет массовое создание заданий на обзвон

import requests
import pytest
import json
from apitest.calltask.variables_calltask import PhoneNumber, SipHeaders, Tag, DateToCall

# переводим данные для звонка в формат json
data_other1 = json.dumps({'name': 'Евгений', 'code': '123456'})
data_other2 = json.dumps({'name': 'Кирилл', 'code': '654321'})
data_for_call = json.dumps([
    {'phone': PhoneNumber.phone1, 'sip_headers': SipHeaders.sip_headers_bulk1, 'data': data_other1},
    {'phone': PhoneNumber.phone2, 'sip_headers': SipHeaders.sip_headers_bulk2, 'data': data_other2},
])


# Тест массого создания заданий на обзвон
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_create_call_bulk(env_url_create_bulk, env_api_key, env_project_id):  # создаем тестовую функцию

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'data': data_for_call,
            'tag': Tag.tag_create_bulk}  # готовим данные для запроса
    res = requests.post(env_url_create_bulk, data=data)  # передаем параметры в запрос
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа (до 3 секунд), формат ответа: параметры bulk_id и call_task_id
    # и формат этих параметров
    assert res.status_code == 200, "status code is not 200"  # проверяем, что код 200. Если не 200, то выводим сообщение
    assert type(res_json["bulk_id"]) is int, "answer type is not a numeric"  # проверяем что в ответе есть bulk_id и это число
    assert type(res_json["0"]["data"]["call_task_id"]) is int, "answer type is not a numeric"  # проверяем, что "call_task_id" это число.
    assert type(res_json["1"]["data"]["call_task_id"]) is int, "answer type is not a numeric"


# тест валидации параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key_bulk(env_url_create_bulk, env_api_key_fail, env_project_id):

    # готовим данные, тправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# тест проверяет валидацию параметров api_key и project_id
@pytest.mark.negative
@pytest.mark.api
def test_invalid_project_id_bulk(env_url_create_bulk, env_api_key, env_project_id_fail):

    # готовим данные, тправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id_fail, 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# валидация поля phone
@pytest.mark.negative
@pytest.mark.api
def test_empty_phone_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'data': json.dumps([{'phone': ''}, {'phone': ''}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["errors"][0]["title"] == 'required_phone', "другой ответ"
    assert res_json[1]["errors"][0]["title"] == 'required_phone', "другой ответ"


# тест валидации параметра project_id.
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id_bulk(env_url_create_bulk, env_api_key):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': '', 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_project_id', "другой ответ"


# тест валидации параметра api_key.
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key_bulk(env_url_create_bulk, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id, 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_api_key', "другой ответ"


# тест валидации параметра api_key.
@pytest.mark.negative
@pytest.mark.api
def test_wrong_api_key_bulk(env_url_create_bulk, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'invalid_api_key', 'project_id': env_project_id, 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# тест валидации параметра data.
@pytest.mark.negative
@pytest.mark.api
def test_invalid_data_for_call_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'data': json.dumps([{'phone': PhoneNumber.phone1, 'data': {'name': 'Евгений', 'code': '123456'}},
                                {'phone': PhoneNumber.phone2, 'data': {'name': 'Кирилл', 'code': '654321'}}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["errors"][0]["title"] == 'json_data', "другой ответ"
    assert res_json[1]["errors"][0]["title"] == 'json_data', "другой ответ"


# тест валидации параметра min_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_invalid_min_datetime_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'data': json.dumps([{'phone': PhoneNumber.phone1, "min_datetime_to_call": DateToCall.invalid_datetime},
                                {'phone': PhoneNumber.phone2, "min_datetime_to_call": DateToCall.invalid_datetime}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["errors"][0]["title"] == 'date_min_datetime_to_call', "другой ответ"
    assert res_json[1]["errors"][0]["title"] == 'date_min_datetime_to_call', "другой ответ"


# тест валидации параметра max_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_invalid_max_datetime_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'data': json.dumps([{'phone': PhoneNumber.phone1, "max_datetime_to_call": DateToCall.invalid_datetime},
                                {'phone': PhoneNumber.phone2, "max_datetime_to_call": DateToCall.invalid_datetime}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["errors"][0]["title"] == 'date_max_datetime_to_call', "другой ответ"
    assert res_json[1]["errors"][0]["title"] == 'date_max_datetime_to_call', "другой ответ"


# тест валидации параметра tag
@pytest.mark.negative
@pytest.mark.api
def test_invalid_tag_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'tag': '', 'data': data_for_call}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'string_tag', "другой ответ"


# тест валидации параметра data
@pytest.mark.negative
@pytest.mark.api
def test_empty_data_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'data': ''}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_data', "другой ответ"


# тест валидации параметра sip_headers
@pytest.mark.negative
@pytest.mark.api
def test_invalid_sip_headers_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'data': json.dumps([{'phone': PhoneNumber.phone1, 'sip_headers': 'test'},
                                {'phone': PhoneNumber.phone2, 'sip_headers': 'test'}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["errors"][0]["title"] == 'json_sip_headers', "другой ответ"
    assert res_json[1]["errors"][0]["title"] == 'json_sip_headers', "другой ответ"


# тест создания 10 001 звонка
@pytest.mark.negative
@pytest.mark.api
def test_create_call_10k_bulk(env_url_create_bulk, env_api_key_10k, env_project_id):

    # создаем пустой список и закидываем в него 10 001 заданий на звонок.
    data_10k = []
    for i in range(10001):
        data_10k.append({'phone': PhoneNumber.phone1})

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_10k, 'project_id': env_project_id, 'data': json.dumps(data_10k)}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["details"] == 'data length is over 10000', "другой ответ"


# тест проверяет сочетание различных параметров и ошибок в запросе. Всего 6 заданий, из них валидный только один.
@pytest.mark.positive
@pytest.mark.api
def test_partial_create_call_bulk(env_url_create_bulk, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'data': json.dumps([
                {'phone': PhoneNumber.phone2},
                {'phone': ''},
                {'phone': PhoneNumber.phone2, 'sip_headers': 'sip_headers'},
                {'phone': PhoneNumber.phone2, 'min_datetime_to_call': DateToCall.invalid_datetime},
                {'phone': PhoneNumber.phone2, 'max_datetime_to_call': DateToCall.invalid_datetime},
                {'phone': PhoneNumber.phone2, 'data': {'name': 'Кирилл', 'code': '654321'}}])}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа формат ответа
    # из 6 заданий создается только одно, остальные 5 создаются с ошибкой
    assert res.status_code == 200, "код ответа не 200"
    assert res.elapsed.total_seconds() <= 3, "ответ дольше 3 секунд"
    assert type(res_json["bulk_id"]) is int, "не число"
    assert type(res_json["0"]["data"]["call_task_id"]) is int, "не число"
    assert res_json["1"]["errors"][0]["title"] == "required_phone", "другой ответ"
    assert res_json["2"]["errors"][0]["title"] == "json_sip_headers", "другой ответ"
    assert res_json["3"]["errors"][0]["title"] == "date_min_datetime_to_call", "другой ответ"
    assert res_json["4"]["errors"][0]["title"] == "date_max_datetime_to_call", "другой ответ"
    assert res_json["5"]["errors"][0]["title"] == "json_data", "другой ответ"


# Тест создания 10 000 звонков
@pytest.mark.positive
@pytest.mark.api
def test_create_9999_call_bulk(env_url_create_bulk, env_api_key_10k, env_project_id):

    # создаем пустой список и закидываем в него 10 000 заданий на звонок.
    data_9k = []
    for i in range(10000):
        data_9k.append({'phone': PhoneNumber.phone1})

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_10k, 'project_id': env_project_id, 'data': json.dumps(data_9k)}
    res = requests.post(env_url_create_bulk, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа формат ответа
    assert res.status_code == 200, "код ответа не 200"
    assert res.elapsed.total_seconds() <= 10, "ответ дольше 10 секунд"
    assert type(res_json["bulk_id"]) is int, "не число"
    for i in range(9999):
        assert type(res_json[str(i)]["data"]["call_task_id"]) is int, "не число"
