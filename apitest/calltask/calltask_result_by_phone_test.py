
# Тест проверяет получение информации о звонках по номеру телефона (для исходящих)

import requests
import pytest
import time
from apitest.calltask.variables_calltask import PhoneNumber


# тест получение информации о звонках по номеру телефона
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_phone(env_url_result_by_phone, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, данные по звонкам, указанным в запросе (по номеру телефона)
    assert res.status_code == 200, "status code is not 200"
    for call in range(len(res_json)):
        assert res_json[call]["data"]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"


# Тест проверяет валидацию параметра api_key (пустое поле)
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key(env_url_result_by_phone, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_api_key', "другой ответ"


# Тест проверяет валидацию параметра api_key (невалидное значение)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_url_result_by_phone, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'api_key', 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# Тест проверяет валидацию параметра project_id (пустое поле)
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id(env_url_result_by_phone, env_api_key):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': '', 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_project_id', "другой ответ"


# тест валидации параметра api_key. Используются данные для параметризации data_project_id_fail
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_url_result_by_phone, env_api_key_fail, env_project_id):

    # отправляем запрос и переводим ответ в формат json (данные для запроса переданы в параметрах)
    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# Тест проверяет валидацию параметра project_id
@pytest.mark.negative
@pytest.mark.api
def test_invalid_project_id(env_url_result_by_phone, env_api_key, env_project_id_fail):

    # отправляем запрос и переводим ответ в формат json (данные для запроса переданы в параметрах)
    data = {'api_key': env_api_key, 'project_id': env_project_id_fail, 'phone': PhoneNumber.phone1}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# Тест проверяет валидацию параметра phone (пустое поле)
@pytest.mark.negative
@pytest.mark.api
def test_empty_phone(env_url_result_by_phone, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': ''}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_phone', "другой ответ"


# Тест проверяет валидацию параметра phone (несуществующий номер)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_phone(env_url_result_by_phone, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': '70000000000'}
    res = requests.get(env_url_result_by_phone, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == 'no_info_about_this_call', "другой ответ"


# тест проверяет получение инфомации о новом звонке по phone (звонок запускается до запуска теста и
# передает параметры для проверки в тест). Используется фикстура create_new_call_task_bulk из файла conftest.py
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_new_call_check_by_phone(create_new_call_task_bulk, env_url_result_by_phone, env_api_key, env_project_id):

    # принимаем параметры res_call_json, rand_pass1, rand_pass2 из фикстуры
    res_call_json, rand_pass1, rand_pass2 = create_new_call_task_bulk

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    # запускаем цикл, который каждые 2 секунды стучится в апи и проверяет подтянулась инфа по звонку или нет
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    for i in range(90):
        res = requests.get(env_url_result_by_phone, params=data)
        res_json = res.json()
        if res_json[0]['data']['data']['number'] != rand_pass1:
            time.sleep(2)
        else:
            break

    # когда инфа подтянулась проверяем ответ: код ответа, время ответа. В ответе проверяем статус звонка и
    # код (по сути пароль), который передали в запросе.
    assert res.status_code == 200, "status code is not 200"
    assert res_json[0]["data"]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json[0]["data"]["status"] == 22, "wrong answer"
    assert res_json[0]['data']['data']['number'] == rand_pass1, "wrong answer"
