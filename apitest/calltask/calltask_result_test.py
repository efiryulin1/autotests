
# Тест проверяет получение информации о звонках по call_task_id

import requests
import pytest
import time
from apitest.calltask.variables_calltask import CallTask, PhoneNumber, DateToCall


# тест получение информации о звонках по call_task_ids
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_result_by_call_task_id(env_url_result, env_api_key, env_project_id, env_call_task, env_date_call_task):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа и данные по звонкам, указанным в запросе (телефон и дата звонка)
    assert res.status_code == 200, "status code is not 200"
    assert res_json["data"]["phone_number"] == str(PhoneNumber.phone1), "wrong answer"
    assert res_json["data"]["init_time"] == env_date_call_task, "wrong answer"


# тест валидации параметра project_id
@pytest.mark.negative
@pytest.mark.api
def test_wrong_project_id(env_url_result, env_api_key, env_project_id_fail, env_call_task):

    # отправляем запрос и переводим ответ в формат json (данные для запроса переданы в параметрах)
    data = {'api_key': env_api_key, 'project_id': env_project_id_fail, 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "project_id_fail", "другой ответ"


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_wrong_api_key(env_url_result, env_api_key_fail, env_project_id, env_call_task, env_date_call_task):
    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "project_id_fail", "другой ответ"


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key(env_url_result, env_project_id, env_call_task):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id, 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "required_api_key", "другой ответ"


# Тест проверяет валидацию параметра api_key (невалидное значение)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_url_result, env_project_id, env_call_task):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'api_key', 'project_id': env_project_id, 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == "unknown_api_key", "другой ответ"


# Тест проверяет валидацию параметра project_id (пустое поле)
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id(env_url_result, env_api_key, env_call_task):
    data = {'api_key': env_api_key, 'project_id': '', 'call_task_id': env_call_task}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "required_project_id", "другой ответ"


# Тест проверяет валидацию параметра call_task_id (пустое поле)
@pytest.mark.negative
@pytest.mark.api
def test_empty_call_task_id(env_url_result, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': ''}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "required_call_task_id", "другой ответ"


# Тест проверяет валидацию параметра call_task_id (невалидное значение)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_type_call_task_id(env_url_result, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': 'call_task_id'}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == "numeric_call_task_id", "другой ответ"


# Тест проверяет валидацию параметра call_task_id (несуществующее значение)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_call_task_id(env_url_result, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': '00000000'}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# Тест проверяет валидацию параметра call_task_id (значение из другого проекта)
@pytest.mark.negative
@pytest.mark.api
def test_wrong_call_task_id(env_url_result, env_api_key, env_project_id):  # тест падает, т.к. есть баг из-за которого можно получить инфу по звонку с другого проекта

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id,
            'call_task_id': CallTask.wrong_call_task_id}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "unknown_call_task_id", "другой ответ"


# тест валидации параметра min_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_not_called_yet(env_url_create, env_url_result, env_api_key, env_project_id):

    # отправляем запрос на создание звонка
    data_for_call = {'api_key': env_api_key, 'project_id': env_project_id,
                     'phone': PhoneNumber.phone1, 'min_datetime_to_call': DateToCall.min_datetime_to_call}
    res_call = requests.post(env_url_create, data=data_for_call)
    res_call_json = res_call.json()
    res_call_id = res_call_json["data"]["call_task_id"]

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': res_call_id}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"]["title"] == "not_called_yet", "другой ответ"


# тест валидации параметра max_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_time_is_up(env_url_create, env_url_result, env_api_key, env_project_id):

    # отправляем запрос на создание звонка
    data_for_call = {'api_key': env_api_key, 'project_id': env_project_id,
                     'phone': PhoneNumber.phone1, 'max_datetime_to_call': DateToCall.max_datetime_to_call}
    res_call = requests.post(env_url_create, data=data_for_call)
    res_call_json = res_call.json()
    res_call_id = res_call_json["data"]["call_task_id"]

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': res_call_id}
    res = requests.get(env_url_result, params=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["error"]["title"] == "time_is_up", "другой ответ"


# тест проверяет получение инфомации о новом звонке по call_task_id (звонок запускается до запуска теста и
# передает параметры для проверки в тест). Используется фикстура create_new_call_task_bulk из файла conftest.py
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_new_call_check(create_new_call_task, env_url_result, env_api_key, env_project_id):

    # принимаем параметры res_call_json, rand_pass1, rand_pass2 из фикстуры
    res_call_json, rand_pass1 = create_new_call_task
    res_call_id_1 = res_call_json["data"]["call_task_id"]

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    # запускаем цикл, который каждые 2 секунды стучится в апи и проверяет подтянулась инфа по звонку или нет
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'call_task_id': res_call_id_1}
    for i in range(50):
        res = requests.get(env_url_result, params=data)
        if res.status_code == 400:
            time.sleep(5)
        else:
            break
    res_json = res.json()

    # когда инфа подтянулась проверяем ответ: код ответа, время ответа. В ответе проверяем статус звонка и
    # код (по сути пароль), который передали в запросе.
    assert res.status_code == 200, "status code is not 200"
    assert res_json['data']['status'] == 22, "wrong status"
    assert res_json['data']['data']['number'] == rand_pass1, "wrong answer"

