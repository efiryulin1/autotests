
# Тест проверяет создание заданий на обзвон

import requests
import pytest
import json
from apitest.calltask.variables_calltask import PhoneNumber, SipHeaders, Tag, DateToCall


# тест создания звонка
@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_create_call(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data_for_call = json.dumps({'name': 'Евгений', 'code': '123456'})
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'tag': Tag.tag_create, 'data': data_for_call, 'sip_headers': SipHeaders.sip_headers}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа, время ответа (до 3 секунд), формат ответа: параметр call_task_id
    # и его формат
    assert res.status_code == 200, "status code is not 200" # проверяем, что код ответа 200. Если не 200, то выводим сообщение
    assert type(res_json["data"]["call_task_id"]) is int, "answer type is not a numeric"  # проверяем, что "call_task_id" это число.


# тест валидации параметра project_id
@pytest.mark.negative
@pytest.mark.api
def test_invalid_project_id(env_url_create, env_api_key, env_project_id_fail):

    # отправляем запрос и переводим ответ в формат json (данные для запроса переданы в параметрах)
    data = {'api_key': env_api_key, 'project_id': env_project_id_fail, 'phone': PhoneNumber.phone1}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# тест валидации параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_url_create, env_api_key_fail, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'project_id_fail', "другой ответ"


# тест валидации параметра phone.
@pytest.mark.negative
@pytest.mark.api
def test_empty_phone(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': ''}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_phone', "другой ответ"


# тест валидации параметра project_id.
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id(env_url_create, env_api_key):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': '', 'phone': PhoneNumber.phone1}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_project_id', "другой ответ"


# тест валидации параметра api_key.
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key(env_url_create, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': '', 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'required_api_key', "другой ответ"


# тест валидации параметра api_key.
@pytest.mark.negative
@pytest.mark.api
def test_incorrect_api_key(env_url_create, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': 'incorrect-key', 'project_id': env_project_id, 'phone': PhoneNumber.phone1}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 403, "код ответа не 403"
    assert res_json["errors"]["title"] == 'unknown_api_key', "другой ответ"


# тест валидации параметра tag
@pytest.mark.negative
@pytest.mark.api
def test_invalid_tag(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'tag': ''}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'string_tag', "другой ответ"


# тест валидации параметра sip_headers
@pytest.mark.negative
@pytest.mark.api
def test_empty_sip_headers(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'sip_headers': ''}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'json_sip_headers', "другой ответ"


# тест валидации параметра min_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_invalid_min_datetime_to_call(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'min_datetime_to_call': DateToCall.invalid_datetime}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'date_min_datetime_to_call', "другой ответ"


# тест валидации параметра max_datetime_to_call
@pytest.mark.negative
@pytest.mark.api
def test_invalid_max_datetime_to_call(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'max_datetime_to_call': DateToCall.invalid_datetime}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'date_max_datetime_to_call', "другой ответ"


# тест валидации параметра data
@pytest.mark.negative
@pytest.mark.api
def test_invalid_data_for_call(env_url_create, env_api_key, env_project_id):

    # готовим данные для запроса, отправляем запрос и переводим ответ в формат json
    data = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
            'data': ''}
    res = requests.post(env_url_create, data=data)
    res_json = res.json()

    # блок проверок: проверяем код ответа и текст ошибки
    assert res.status_code == 400, "код ответа не 400"
    assert res_json["errors"][0]["title"] == 'json_data', "другой ответ"
