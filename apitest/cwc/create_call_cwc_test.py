import pytest
import requests
import time
from variables_cwc import URL, Phone, Scenario, Client, TimeZone, ApiKey, ProjectId, Headers


@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_create_call():
    data = {'number': Phone.phone, 'client': Client.client, 'time_zone': TimeZone.time_zone,
            'scenario': Scenario.scenario}
    res = requests.get(URL.create_call_cwc_url, params=data, headers=Headers.header_auth)
    res_json = res.json()

    assert res.status_code == 200, "status code is not 200"
    assert type(res_json["data"]["call_task_id"]) is int, "answer type is not a numeric"


@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_check_call_cwc():
    data_create_call = {'number': Phone.phone, 'client': Client.client, 'time_zone': TimeZone.time_zone,
                        'scenario': Scenario.scenario}
    create_call = requests.get(URL.create_call_cwc_url, params=data_create_call, headers=Headers.header_auth)
    create_call_json = create_call.json()
    call_id_cwc = create_call_json["data"]["call_task_id"]

    data_result = {'api_key': ApiKey.api_key_cwc, 'project_id': ProjectId.project_id_cwc,
                   'call_task_id': call_id_cwc}
    for i in range(90):
        result = requests.get(URL.calltask_result_url_prod, params=data_result)
        if result.status_code == 400:
            time.sleep(2)
        else:
            break
    result_json = result.json()

    assert result.status_code == 200, "status code is not 200"
    assert result_json["data"]["phone_number"] == Phone.phone, "wrong phone"
