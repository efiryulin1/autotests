import pytest
import requests
from variables_cwc import URL, Phone, Headers, ApiKey, ProjectId


@pytest.mark.positive
@pytest.mark.api
@pytest.mark.smoke
def test_get_context():
    data = {'api_key': ApiKey.api_key_cwc, 'project_id': ProjectId.project_id_cwc, 'number': Phone.phone}
    result = requests.get(URL.get_context_cwc_url, params=data, headers=Headers.header_auth)
    result_json = result.json()

    assert result.status_code == 200, "status code is not 200"
    assert result_json['number'] == Phone.phone, "wrong phone"
