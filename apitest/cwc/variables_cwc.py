class URL:
    create_call_cwc_url = 'https://go.robotmia.ru/api/cwc/v2/startCall'
    get_context_cwc_url = 'https://go.robotmia.ru/api/cwc/v2/getContext'
    calltask_result_url_prod = 'https://go.robotmia.ru/api/calltask/result'



class ApiKey:
    api_key_cwc = '14812fbe-5bc2-403e-8a21-5eddc8be9127'


class ProjectId:
    project_id_cwc = '17'


class Headers:
    header_auth = {"Authorization": "Basic Y3djOmN3YzM0ZXJkZg=="}


class Phone:
    phone = '79231265383'


class Scenario:
    scenario = "cwc"


class Client:
    client = 12345


class TimeZone:
    time_zone = 7
