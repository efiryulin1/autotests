
# Тест получения данных из кастомной таблицы

import pytest
import requests
from variable_analytics import DateTime, CardId


# Тест получает данные о звонках с 172 проекта за период с 01.01.23 по 31.01.23 (для prod).
# Тест получения данных из кастомной таблицы из 13 проекта за период с 01.11.22 по 31.11.22
@pytest.mark.positive
@pytest.mark.api
def test_get_custom_card(env_api_key_analytics, env_project_id, env_get_custom_card_call, env_date_range_from, env_date_range_to,
                         env_get_custom_card_url, env_number_of_calls, env_number_of_calls_status21,
                         env_number_of_calls_status22, env_number_of_calls_status23):

    # готовим данные для запроса
    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}

    # передаем запрос с параметрами
    res = requests.get(env_get_custom_card_url, params=data)

    # переводим ответ в json формат (нужно для последующих проверок)
    res_json = res.json()

    # Блок проверок. Проверяем код ответа, кол-во строк в ответе данные в каждой колонке
    assert res.status_code == 200, "код ответа не 200"
    assert len(res_json) == 4, "другое количество строк"
    assert res_json[0]["column_1"] == 'Всего звонков'
    assert res_json[0]["column_2"] == str(env_number_of_calls)
    assert res_json[1]["column_1"] == 'Автоответчик'
    assert res_json[1]["column_2"] == str(env_number_of_calls_status22)
    assert res_json[2]["column_1"] == 'Окончен по сценарию'
    assert res_json[2]["column_2"] == str(env_number_of_calls_status23)
    assert res_json[3]["column_1"] == 'Окончен абонентом'
    assert res_json[3]["column_2"] == str(env_number_of_calls_status21)


# Тест получения данных из кастомной таблицы из 9 проекта (чаты) за период с 01.11.22 по 31.11.22.
# Аналогичен тесту по звонкам
@pytest.mark.positive
@pytest.mark.api
def test_get_custom_card_chat(env_api_key_chat, env_get_custom_card_chat, env_date_range_from, env_date_range_to,
                              env_get_custom_card_url, env_project_id_chat_analytics):
    data = {'api_key': env_api_key_chat, 'project_id': env_project_id_chat_analytics, 'card_id': env_get_custom_card_chat,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    assert res.status_code == 200, "код ответа не 200"
    assert len(res_json) == 3, "другое количество строк"
    assert res_json[0]["column_1"] == 'Чаты'
    assert res_json[0]["column_2"] == '0'
    assert res_json[1]["column_1"] == 'Чаты'
    assert res_json[1]["column_2"] == '0'
    assert res_json[2]["column_1"] == 'Чаты'
    assert res_json[2]["column_2"] == '0'


# Тест проверяет валидацию параметра project_id
@pytest.mark.negative
@pytest.mark.api
def test_empty_project_id(env_api_key_analytics, env_get_custom_card_call, env_date_range_from, env_date_range_to, env_get_custom_card_url):
    data = {'api_key': env_api_key_analytics, 'project_id': '', 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]':env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["errors"][0]["title"] == 'required_project_id'


# Тест проверяет валидацию параметра project_id
# @pytest.mark.negative
# @pytest.mark.api
# def test_numeric_project_id(env_api_key_analytics, env_get_custom_card_call, env_date_range_from, env_date_range_to, env_get_custom_card_url):
#     data = {'api_key': env_api_key_analytics, 'project_id': 'project_id', 'card_id': env_get_custom_card_call,
#             'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
#             'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
#     res = requests.get(env_get_custom_card_url, params=data)
#     res_json = res.json()
#
#     # Проверяем код ответа и текст ошибки
#     assert res.status_code == 400
#     assert res_json["errors"][0]["title"] == 'numeric_project_id'


# Тест проверяет валидацию параметра api_key
@pytest.mark.negative
@pytest.mark.api
def test_empty_api_key(env_project_id, env_get_custom_card_call, env_date_range_from, env_date_range_to, env_get_custom_card_url):
    data = {'api_key': '', 'project_id': env_project_id, 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["errors"][0]["title"] == 'required_api_key'


# Тест проверяет валидацию сочетания параметров project_id и api_key (если пара невалидна, то должно возвращать ошибку)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_project_id, env_get_custom_card_call, env_date_range_from, env_date_range_to,
                         env_get_custom_card_url):

    data = {'api_key': 'api_key', 'project_id': env_project_id, 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 403
    assert res_json["errors"]["title"] == 'unknown_api_key'


# Тест проверяет валидацию сочетания параметров project_id и api_key (если пара невалидна, то должно возвращать ошибку)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_api_key(env_api_key_fail, env_project_id, env_get_custom_card_call, env_date_range_from,
                         env_date_range_to, env_get_custom_card_url):

    data = {'api_key': env_api_key_fail, 'project_id': env_project_id, 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 403
    assert res_json["errors"]["title"] == 'unknown_api_key'


# Тест проверяет валидацию сочетания параметров project_id и api_key (если пара невалидна, то должно возвращать ошибку)
@pytest.mark.negative
@pytest.mark.api
def test_invalid_project_id(env_api_key_analytics, env_project_id_fail, env_get_custom_card_call, env_date_range_from,
                            env_date_range_to, env_get_custom_card_url):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id_fail, 'card_id': env_get_custom_card_call,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 403
    assert res_json["errors"]["title"] == 'unknown_api_key'


# Тест проверяет валидацию параметра card_id
@pytest.mark.negative
@pytest.mark.api
def test_empty_card_id(env_api_key_analytics, env_project_id, env_date_range_from, env_date_range_to, env_get_custom_card_url):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': '',
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["errors"][0]["title"] == 'required_card_id'


# Тест проверяет валидацию параметра card_id
@pytest.mark.negative
@pytest.mark.api
def test_numeric_card_id(env_api_key_analytics, env_project_id, env_date_range_from, env_date_range_to, env_get_custom_card_url):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': 'card_id',
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["errors"][0]["title"] == 'numeric_card_id'


# Тест проверяет получение информции по карте с другого прокта, удаленной карте и несуществующей карте
@pytest.mark.negative
@pytest.mark.api
def test_wrong_card_id(env_api_key_analytics, env_project_id, env_date_range_from, env_date_range_to, env_get_custom_card_url,
                       env_get_custom_card_chat):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': env_get_custom_card_chat,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["error"] == 'Card was not found'


# Тест проверяет получение информции по карте с другого прокта, удаленной карте и несуществующей карте
@pytest.mark.negative
@pytest.mark.api
def test_wrong_card_id(env_api_key_analytics, env_project_id, env_date_range_from, env_date_range_to, env_get_custom_card_url):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': CardId.card_id_not_exist,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["error"] == 'Card was not found'


# Тест проверяет получение информции по карте с другого прокта, удаленной карте и несуществующей карте
@pytest.mark.negative
@pytest.mark.api
def test_wrong_card_id(env_api_key_analytics, env_project_id, env_date_range_from, env_date_range_to, env_get_custom_card_url,
                       env_delete_card_id):

    data = {'api_key': env_api_key_analytics, 'project_id': env_project_id, 'card_id': env_delete_card_id,
            'date_range[date_from]': env_date_range_from, 'date_range[date_to]': env_date_range_to,
            'date_range[time_from]': DateTime.time_from, 'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_custom_card_url, params=data)
    res_json = res.json()

    # Проверяем код ответа и текст ошибки
    assert res.status_code == 400
    assert res_json["error"] == 'Card was not found'
