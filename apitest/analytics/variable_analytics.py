
# Данные для запроса, разделены на классы. Используется в тестах аналитики

import datetime


class URLAnalitycs:

    # DEV
    custom_card_url_dev = 'https://godev.robotmia.ru/api/analytics/custom-card'
    call_table_url_dev = 'https://godev.robotmia.ru/api/analytics/calls-table'

    # PROD
    custom_card_url_prod = 'https://go.robotmia.ru/api/analytics/custom-card'
    call_table_url_prod = 'https://go.robotmia.ru/api/analytics/calls-table'


class ProjectIdAnalytics:

    # DEV
    project_id_1 = '1'
    project_id_9 = '9'
    project_id_13 = '13'

    # PROD
    project_id_5 = '5'
    project_id_131 = '131'
    project_id_172 = '172'


class ApiKeyAnalytics:

    # DEV
    api_key_4 = '012a1d24-9b40-412a-bf73-d5c671329e23'
    api_key_9 = 'a0d53c9c-34c4-4c39-a1f3-7e214cab5e0a'
    api_key_13 = 'bdbb6aa0-6196-4e96-8fa5-dd6442cae917'

    # PROD
    api_key_172 = 'c70490df-299c-4631-acae-8fa7d670ec59'
    api_key_131 = '3c085662-443b-4d52-ba63-6f86dd2282f8'


class DateTime:
    date_today = datetime.date.today().strftime("%d.%m.%Y")
    time_from = '00:00:00'
    time_to = '23:59:59'

    # DEV
    date_from_dev = '01.11.2022'
    date_to_dev = '30.11.2022'

    # PROD
    date_from_prod = '01.01.2023'
    date_to_prod = '31.01.2023'


class CardId:
    card_id_not_exist = '999999'

    # DEV
    card_id_4 = '13'
    card_id_9 = '37'
    card_id_13 = '47'
    card_id_delete_dev = '45'

    # PROD
    card_id_131 = '358'
    card_id_172 = '357'
    card_id_delete_prod = '359'


class Headers:
    call_table_header_dev = {"Authorization": "Basic dXNlcl9jbGllbnQ6QSFCQEMjRCRFJQ=="}
    call_table_header_prod = {"Authorization": "Basic dXNlcl9jbGllbnQ6QSFCQEMjRCRFJQ=="}


class Phone:
    phone = '79231265383'


class NumberOfCalls:
    # DEV
    number_of_calls_dev = 356
    number_of_calls_status21_dev = 0
    number_of_calls_status22_dev = 289
    number_of_calls_status23_dev = 67

    # PROD
    number_of_calls_prod = 67
    number_of_calls_status21_prod = 0
    number_of_calls_status22_prod = 56
    number_of_calls_status23_prod = 10

