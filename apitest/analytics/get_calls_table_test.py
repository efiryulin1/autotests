
# Тест на получение данных из "Таблицы звонков" из аналитики

# импортируем нужные билблиотеки и данные из из файла с переменными
import pytest
import requests
import time
from variable_analytics import DateTime, Phone


# Тест получает данные о звонках с 172 проекта за период с 01.01.23 по 31.01.23 (для prod).
# Тест получает данные о звонках с 13 проекта за период с 01.11.22 по 31.11.22 (для dev).
@pytest.mark.positive
@pytest.mark.api
def test_get_calls_table(env_date_range_from, env_date_range_to, env_headers, env_number_of_calls, env_project_id,
                         env_get_calls_table_url):

    # готовим данные для запроса
    data = {'project_id': env_project_id, 'date_range[date_from]': env_date_range_from,
            'date_range[date_to]': env_date_range_to, 'date_range[time_from]': DateTime.time_from,
            'date_range[time_to]': DateTime.time_to}

    # передаем запрос с параметрами
    res = requests.get(env_get_calls_table_url, params=data, headers=env_headers)

    # переводим ответ в json формат (нужно для последующих проверок)
    res_json = res.json()

    # Блок проверок. Проверяем код ответа, кол-во звонков в ответе и звонки в ответе по шаблону
    assert res.status_code == 200, "код ответа не 200"
    assert len(res_json) == env_number_of_calls, "другое количество звонков"
    for call in range(len(res_json)):
        assert type(res_json[call]["id"]) is str
        assert type(res_json[call]["phone_number"]) is str
        assert type(res_json[call]["robot_duration"]) is int
        assert type(res_json[call]["total_duration"]) is int or res_json[call]["total_duration"] is None
        assert type(res_json[call]["init_time"]) is str
        assert type(res_json[call]["status"]) is str
        assert res_json[call]["forward_number"] is None
        assert type(res_json[call]["run_item_id"]) is int or res_json[call]["run_item_id"] is None
        assert res_json[call]["price"] == 0
        assert res_json[call]["rate"] is None


# Тест проверяет отсутствие возможности получить инфо по звонкам к проекту, к которому нет достпуа у пользователя
# (для этого указываем проект, к которому у нас нет доступа)
@pytest.mark.negative
@pytest.mark.api
def test_no_access_to_project(env_date_range_from, env_date_range_to, env_headers, env_project_id_fail_analytics,
                              env_get_calls_table_url):
    data = {'project_id': env_project_id_fail_analytics, 'date_range[date_from]': env_date_range_from,
            'date_range[date_to]': env_date_range_to, 'date_range[time_from]': DateTime.time_from,
            'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_calls_table_url, params=data, headers=env_headers)

    # в ответе должен прийти код 403, означающий отсутвие достпуа к проекту
    assert res.status_code == 403, "код ответа не 403"


# Тест проверяет возможность получение информации о звонках для неавторизованного пользователя (для этого не передаем
# параметры авторизации)
@pytest.mark.negative
@pytest.mark.api
def test_unauthorized(env_date_range_from, env_date_range_to, env_project_id, env_get_calls_table_url):
    data = {'project_id': env_project_id, 'date_range[date_from]': env_date_range_from,
            'date_range[date_to]': env_date_range_to, 'date_range[time_from]': DateTime.time_from,
            'date_range[time_to]': DateTime.time_to}
    res = requests.get(env_get_calls_table_url, params=data)

    # в ответе должен прийти код 401, означающий что пользователь не авторизован
    assert res.status_code == 401, "код ответа не 401"


# Тест проверяет добавление нового звонка в таблицу звонков.
# Используется фикстура создания звонка из conftest.py: create_new_call_task_bulk
# Звонок запускается до запуска теста и передает данные в тест для проверки
@pytest.mark.positive
@pytest.mark.api
def test_last_call_check_calls_table(create_new_call_task_bulk, env_headers, env_project_id, env_get_calls_table_url):

    # параметры переданные из фикстуры
    res_call_json, rand_pass1, rand_pass2 = create_new_call_task_bulk
    res_call_id_1 = res_call_json["0"]["data"]["call_task_id"]

    # данные для запроса
    data = {'project_id': env_project_id, 'date_range[date_from]': DateTime.date_today,
            'date_range[date_to]': DateTime.date_today, 'date_range[time_from]': DateTime.time_from,
            'date_range[time_to]': DateTime.time_to}

    # Посылаем запрос в API, для проверки подтянуись данные или нет. КОгда подтянулись выходим из цикла
    for i in range(90):
        res = requests.get(env_get_calls_table_url, params=data, headers=env_headers)
        res_json = res.json()
        if res_json[0]["run_item_id"] != res_call_id_1:
            time.sleep(2)
        else:
            break

    # Блок проверок. Проверяем код ответа, номер телефона и run_item_id
    assert res.status_code == 200, "код ответа не 200"
    assert res_json[0]["phone_number"] == Phone.phone, "другой номер"
    assert res_json[0]["run_item_id"] == res_call_id_1, "другой call task"
