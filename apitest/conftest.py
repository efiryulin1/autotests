
# файл содержит фикстуры, которые запускаются до запуска теста (при необходимости)


import pytest
import requests
import random
import json

import apitest.analytics.variable_analytics
from apitest.calltask.variables_calltask import URL, ApiKey, ProjectId, PhoneNumber, Tag, CallTask, DateToCall
from apitest.analytics.variable_analytics import DateTime, Headers, NumberOfCalls, ProjectIdAnalytics, URLAnalitycs, \
    CardId, ApiKeyAnalytics


# функция принимает аргумент ('prod' или 'dev') из командной строки.
# От него зависит окружение, на котором запускается тест
def pytest_addoption(parser):
    parser.addoption(
        '--env', action='store', default='dev', help='available environments: dev, prod'
    )


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url_create(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        calltask_create_url = URL.calltask_create_url_prod
    else:
        calltask_create_url = URL.calltask_create_url_dev
    yield calltask_create_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture(scope="class")
def env_url_create_bulk(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        calltask_create_bulk_url = apitest.calltask.variables_calltask.URL.calltask_create_bulk_url_prod
    else:
        calltask_create_bulk_url = apitest.calltask.variables_calltask.URL.calltask_create_bulk_url_dev
    yield calltask_create_bulk_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url_result(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        calltask_result_url = URL.calltask_result_url_prod
    else:
        calltask_result_url = URL.calltask_result_url_dev
    yield calltask_result_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url_result_bulk(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        calltask_result_bulk_url = URL.calltask_result_bulk_url_prod
    else:
        calltask_result_bulk_url = URL.calltask_result_bulk_url_dev
    yield calltask_result_bulk_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url_result_by_phone(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        calltask_result_by_phone_url = URL.calltask_result_by_phone_url_prod
    else:
        calltask_result_by_phone_url = URL.calltask_result_by_phone_url_dev
    yield calltask_result_by_phone_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url_result_by_phone_in(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        call_result_by_phone_url = URL.call_result_by_phone_url_prod
    else:
        call_result_by_phone_url = URL.call_result_by_phone_url_dev
    yield call_result_by_phone_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_get_calls_table_url(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        get_calls_table_url = apitest.analytics.variable_analytics.URLAnalitycs.call_table_url_prod
    else:
        get_calls_table_url = apitest.analytics.variable_analytics.URLAnalitycs.call_table_url_dev
    yield get_calls_table_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_get_custom_card_url(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        get_custom_card_url = apitest.analytics.variable_analytics.URLAnalitycs.custom_card_url_prod
    else:
        get_custom_card_url = apitest.analytics.variable_analytics.URLAnalitycs.custom_card_url_dev
    yield get_custom_card_url


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture(scope="class")
def env_api_key(request):
    env_api_key = request.config.getoption('--env')

    if env_api_key == 'prod':
        api_key = ApiKey.api_key_project_172
    else:
        api_key = ApiKey.api_key_project_13
    yield api_key


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_api_key_fail(request):
    env_api_key_fail = request.config.getoption('--env')

    if env_api_key_fail == 'prod':
        api_key_fail = ApiKey.api_key_project_4
    else:
        api_key_fail = ApiKey.api_key_project_1_dev
    yield api_key_fail


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_api_key_10k(request):
    env_api_key_10k = request.config.getoption('--env')

    if env_api_key_10k == 'prod':
        api_key = ApiKey.api_key_10k_prod
    else:
        api_key = ApiKey.api_key_10k_dev
    yield api_key


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_api_key_in(request):
    env_api_key_in = request.config.getoption('--env')

    if env_api_key_in == 'prod':
        api_key_in = ApiKey.api_key_project_1_prod
    else:
        api_key_in = ApiKey.api_key_project_6
    yield api_key_in


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_api_key_analytics(request):
    env_api_key_analytics = request.config.getoption('--env')

    if env_api_key_analytics == 'prod':
        api_key_analytics = ApiKeyAnalytics.api_key_172
    else:
        api_key_analytics = ApiKeyAnalytics.api_key_13
    yield api_key_analytics


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_api_key_chat(request):
    env_api_key_chat = request.config.getoption('--env')

    if env_api_key_chat == 'prod':
        api_key_chat = ApiKeyAnalytics.api_key_131
    else:
        api_key_chat = ApiKeyAnalytics.api_key_9
    yield api_key_chat


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture(scope="class")
def env_project_id(request):
    env_project_id = request.config.getoption('--env')

    if env_project_id == 'prod':
        project_id = ProjectId.project_id_172
    else:
        project_id = ProjectId.project_id_13
    yield project_id


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_project_id_fail(request):
    env_project_id_fail = request.config.getoption('--env')

    if env_project_id_fail == 'prod':
        project_id_fail = ProjectId.project_id_4
    else:
        project_id_fail = ProjectId.project_id_1
    yield project_id_fail


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_project_id_fail_analytics(request):
    env_project_id_fail_analytics = request.config.getoption('--env')

    if env_project_id_fail_analytics == 'prod':
        project_id_fail_analytics = ProjectIdAnalytics.project_id_5
    else:
        project_id_fail_analytics = ProjectIdAnalytics.project_id_1
    yield project_id_fail_analytics


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_project_id_in(request):
    env_project_id_in = request.config.getoption('--env')

    if env_project_id_in == 'prod':
        project_id_in = ProjectId.project_id_1
    else:
        project_id_in = ProjectId.project_id_6
    yield project_id_in


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_project_id_chat_analytics(request):
    env_project_id_chat_analytics = request.config.getoption('--env')

    if env_project_id_chat_analytics == 'prod':
        project_id_chat_analytics = ProjectIdAnalytics.project_id_131
    else:
        project_id_chat_analytics = ProjectIdAnalytics.project_id_9
    yield project_id_chat_analytics


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task(request):
    env_call_task = request.config.getoption('--env')

    if env_call_task == 'prod':
        call_task = CallTask.call_task_1_prod
    else:
        call_task = CallTask.call_task_1_dev
    yield call_task


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_1(request):
    env_call_task_bulk_1 = request.config.getoption('--env')

    if env_call_task_bulk_1 == 'prod':
        call_task_bulk_1 = CallTask.call_task_id_1_pr_172
    else:
        call_task_bulk_1 = CallTask.call_task_id_1_pr_13
    yield call_task_bulk_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_3(request):
    env_call_task_bulk_3 = request.config.getoption('--env')

    if env_call_task_bulk_3 == 'prod':
        call_task_bulk_3 = CallTask.call_task_id_3_pr_172
    else:
        call_task_bulk_3 = CallTask.call_task_id_3_pr_13
    yield call_task_bulk_3


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_4(request):
    env_call_task_bulk_4 = request.config.getoption('--env')

    if env_call_task_bulk_4 == 'prod':
        call_task_bulk_4 = CallTask.call_task_id_4_pr_172
    else:
        call_task_bulk_4 = CallTask.call_task_id_4_pr_13
    yield call_task_bulk_4


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_5(request):
    env_call_task_bulk_5 = request.config.getoption('--env')

    if env_call_task_bulk_5 == 'prod':
        call_task_bulk_5 = CallTask.call_task_id_5_pr_172
    else:
        call_task_bulk_5 = CallTask.call_task_id_5_pr_13
    yield call_task_bulk_5


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_6(request):
    env_call_task_bulk_6 = request.config.getoption('--env')

    if env_call_task_bulk_6 == 'prod':
        call_task_bulk_6 = CallTask.call_task_id_6_pr_172
    else:
        call_task_bulk_6 = CallTask.call_task_id_6_pr_13
    yield call_task_bulk_6


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_7(request):
    env_call_task_bulk_7 = request.config.getoption('--env')

    if env_call_task_bulk_7 == 'prod':
        call_task_bulk_7 = CallTask.call_task_id_7_pr_172
    else:
        call_task_bulk_7 = CallTask.call_task_id_7_pr_13
    yield call_task_bulk_7


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_8(request):
    env_call_task_bulk_8 = request.config.getoption('--env')

    if env_call_task_bulk_8 == 'prod':
        call_task_bulk_8 = CallTask.call_task_id_8_pr_172
    else:
        call_task_bulk_8 = CallTask.call_task_id_8_pr_13
    yield call_task_bulk_8


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_9(request):
    env_call_task_bulk_9 = request.config.getoption('--env')

    if env_call_task_bulk_9 == 'prod':
        call_task_bulk_9 = CallTask.call_task_id_9_pr_172
    else:
        call_task_bulk_9 = CallTask.call_task_id_9_pr_13
    yield call_task_bulk_9


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_10(request):
    env_call_task_bulk_10 = request.config.getoption('--env')

    if env_call_task_bulk_10 == 'prod':
        call_task_bulk_10 = CallTask.call_task_id_10_pr_172
    else:
        call_task_bulk_10 = CallTask.call_task_id_10_pr_13
    yield call_task_bulk_10


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_11(request):
    env_call_task_bulk_11 = request.config.getoption('--env')

    if env_call_task_bulk_11 == 'prod':
        call_task_bulk_11 = CallTask.call_task_id_11_pr_172
    else:
        call_task_bulk_11 = CallTask.call_task_id_11_pr_13
    yield call_task_bulk_11


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_12(request):
    env_call_task_bulk_12 = request.config.getoption('--env')

    if env_call_task_bulk_12 == 'prod':
        call_task_bulk_12 = CallTask.call_task_id_12_pr_172
    else:
        call_task_bulk_12 = CallTask.call_task_id_12_pr_13
    yield call_task_bulk_12


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_13(request):
    env_call_task_bulk_13 = request.config.getoption('--env')

    if env_call_task_bulk_13 == 'prod':
        call_task_bulk_13 = CallTask.call_task_id_13_pr_172
    else:
        call_task_bulk_13 = CallTask.call_task_id_13_pr_13
    yield call_task_bulk_13


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_14(request):
    env_call_task_bulk_14 = request.config.getoption('--env')

    if env_call_task_bulk_14 == 'prod':
        call_task_bulk_14 = CallTask.call_task_id_14_pr_172
    else:
        call_task_bulk_14 = CallTask.call_task_id_14_pr_13
    yield call_task_bulk_14


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_15(request):
    env_call_task_bulk_15 = request.config.getoption('--env')

    if env_call_task_bulk_15 == 'prod':
        call_task_bulk_15 = CallTask.call_task_id_15_pr_172
    else:
        call_task_bulk_15 = CallTask.call_task_id_15_pr_13
    yield call_task_bulk_15


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_16(request):
    env_call_task_bulk_16 = request.config.getoption('--env')

    if env_call_task_bulk_16 == 'prod':
        call_task_bulk_16 = CallTask.call_task_id_16_pr_172
    else:
        call_task_bulk_16 = CallTask.call_task_id_16_pr_13
    yield call_task_bulk_16


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_call_task_bulk_fail_1(request):
    env_call_task_bulk_fail_1 = request.config.getoption('--env')

    if env_call_task_bulk_fail_1 == 'prod':
        call_task_bulk_fail_1 = CallTask.call_task_id_1_pr_4
    else:
        call_task_bulk_fail_1 = CallTask.call_task_id_1_pr_1
    yield call_task_bulk_fail_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task(request):
    env_date_call_task = request.config.getoption('--env')

    if env_date_call_task == 'prod':
        date_call_task = DateToCall.date_call_task_1_prod
    else:
        date_call_task = DateToCall.date_call_task_1_dev
    yield date_call_task


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_1(request):
    env_date_call_task_bulk_1 = request.config.getoption('--env')

    if env_date_call_task_bulk_1 == 'prod':
        date_call_task_bulk_1 = DateToCall.date_call_task_bulk_1_prod
    else:
        date_call_task_bulk_1 = DateToCall.date_call_task_bulk_1_dev
    yield date_call_task_bulk_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_3(request):
    env_date_call_task_bulk_3 = request.config.getoption('--env')

    if env_date_call_task_bulk_3 == 'prod':
        date_call_task_bulk_3 = DateToCall.date_call_task_bulk_3_prod
    else:
        date_call_task_bulk_3 = DateToCall.date_call_task_bulk_3_dev
    yield date_call_task_bulk_3


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_4(request):
    env_date_call_task_bulk_4 = request.config.getoption('--env')

    if env_date_call_task_bulk_4 == 'prod':
        date_call_task_bulk_4 = DateToCall.date_call_task_bulk_4_prod
    else:
        date_call_task_bulk_4 = DateToCall.date_call_task_bulk_4_dev
    yield date_call_task_bulk_4


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_5(request):
    env_date_call_task_bulk_5 = request.config.getoption('--env')

    if env_date_call_task_bulk_5 == 'prod':
        date_call_task_bulk_5 = DateToCall.date_call_task_bulk_5_prod
    else:
        date_call_task_bulk_5 = DateToCall.date_call_task_bulk_5_dev
    yield date_call_task_bulk_5


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_7(request):
    env_date_call_task_bulk_7 = request.config.getoption('--env')

    if env_date_call_task_bulk_7 == 'prod':
        date_call_task_bulk_7 = DateToCall.date_call_task_bulk_7_prod
    else:
        date_call_task_bulk_7 = DateToCall.date_call_task_bulk_7_dev
    yield date_call_task_bulk_7


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_9(request):
    env_date_call_task_bulk_9 = request.config.getoption('--env')

    if env_date_call_task_bulk_9 == 'prod':
        date_call_task_bulk_9 = DateToCall.date_call_task_bulk_9_prod
    else:
        date_call_task_bulk_9 = DateToCall.date_call_task_bulk_9_dev
    yield date_call_task_bulk_9


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_10(request):
    env_date_call_task_bulk_10 = request.config.getoption('--env')

    if env_date_call_task_bulk_10 == 'prod':
        date_call_task_bulk_10 = DateToCall.date_call_task_bulk_10_prod
    else:
        date_call_task_bulk_10 = DateToCall.date_call_task_bulk_10_dev
    yield date_call_task_bulk_10


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_11(request):
    env_date_call_task_bulk_11 = request.config.getoption('--env')

    if env_date_call_task_bulk_11 == 'prod':
        date_call_task_bulk_11 = DateToCall.date_call_task_bulk_11_prod
    else:
        date_call_task_bulk_11 = DateToCall.date_call_task_bulk_11_dev
    yield date_call_task_bulk_11


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_12(request):
    env_date_call_task_bulk_12 = request.config.getoption('--env')

    if env_date_call_task_bulk_12 == 'prod':
        date_call_task_bulk_12 = DateToCall.date_call_task_bulk_12_prod
    else:
        date_call_task_bulk_12 = DateToCall.date_call_task_bulk_12_dev
    yield date_call_task_bulk_12


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_13(request):
    env_date_call_task_bulk_13 = request.config.getoption('--env')

    if env_date_call_task_bulk_13 == 'prod':
        date_call_task_bulk_13 = DateToCall.date_call_task_bulk_13_prod
    else:
        date_call_task_bulk_13 = DateToCall.date_call_task_bulk_13_dev
    yield date_call_task_bulk_13


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_call_task_bulk_14(request):
    env_date_call_task_bulk_14 = request.config.getoption('--env')

    if env_date_call_task_bulk_14 == 'prod':
        date_call_task_bulk_14 = DateToCall.date_call_task_bulk_14_prod
    else:
        date_call_task_bulk_14 = DateToCall.date_call_task_bulk_14_dev
    yield date_call_task_bulk_14


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_bulk_1(request):
    env_result_bulk_1 = request.config.getoption('--env')

    if env_result_bulk_1 == 'prod':
        result_bulk_1 = CallTask.bulk_id_1_pr_172
    else:
        result_bulk_1 = CallTask.bulk_id_1_pr_13
    yield result_bulk_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_bulk_2(request):
    env_result_bulk_2 = request.config.getoption('--env')

    if env_result_bulk_2 == 'prod':
        result_bulk_2 = CallTask.bulk_id_2_pr_172
    else:
        result_bulk_2 = CallTask.bulk_id_2_pr_13
    yield result_bulk_2


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_bulk_5(request):
    env_result_bulk_5 = request.config.getoption('--env')

    if env_result_bulk_5 == 'prod':
        result_bulk_5 = CallTask.bulk_id_5_pr_172
    else:
        result_bulk_5 = CallTask.bulk_id_5_pr_13
    yield result_bulk_5


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_bulk_fail_1(request):
    env_bulk_fail_1 = request.config.getoption('--env')

    if env_bulk_fail_1 == 'prod':
        bulk_fail_1 = CallTask.bulk_id_1_pr_4
    else:
        bulk_fail_1 = CallTask.bulk_id_1_pr_1
    yield bulk_fail_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_tag_1(request):
    env_tag_1 = request.config.getoption('--env')

    if env_tag_1 == 'prod':
        tag_1 = Tag.tag_1_pr_172
    else:
        tag_1 = Tag.tag_1_pr_13
    yield tag_1


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_tag_2(request):
    env_tag_2 = request.config.getoption('--env')

    if env_tag_2 == 'prod':
        tag_2 = Tag.tag_2_pr_172
    else:
        tag_2 = Tag.tag_2_pr_13
    yield tag_2


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_tag_3(request):
    env_tag_3 = request.config.getoption('--env')

    if env_tag_3 == 'prod':
        tag_3 = Tag.tag_3_pr_1
    else:
        tag_3 = Tag.tag_3_pr_4
    yield tag_3

# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_tag_4(request):
    env_tag_4 = request.config.getoption('--env')

    if env_tag_4 == 'prod':
        tag_4 = Tag.tag_4_pr_172_pr_4
    else:
        tag_4 = Tag.tag_4_pr_13_pr_1
    yield tag_4


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture(scope="class")
def env_tag_5(request):
    env_tag_5 = request.config.getoption('--env')

    if env_tag_5 == 'prod':
        tag_5 = Tag.tag_5_pr_172
    else:
        tag_5 = Tag.tag_5_pr_13
    yield tag_5


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_range_from(request):
    env_date_range_from = request.config.getoption('--env')

    if env_date_range_from == 'prod':
        date_from = DateTime.date_from_prod
    else:
        date_from = DateTime.date_from_dev
    yield date_from


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_date_range_to(request):
    env_date_range_to = request.config.getoption('--env')

    if env_date_range_to == 'prod':
        date_to = DateTime.date_to_prod
    else:
        date_to = DateTime.date_to_dev
    yield date_to


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_headers(request):
    env_headers = request.config.getoption('--env')

    if env_headers == 'prod':
        headers = Headers.call_table_header_prod
    else:
        headers = Headers.call_table_header_dev
    yield headers


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_number_of_calls(request):
    env_number_of_calls = request.config.getoption('--env')

    if env_number_of_calls == 'prod':
        number_of_calls = NumberOfCalls.number_of_calls_prod
    else:
        number_of_calls = NumberOfCalls.number_of_calls_dev
    yield number_of_calls


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_number_of_calls_status21(request):
    env_number_of_calls_status21 = request.config.getoption('--env')

    if env_number_of_calls_status21 == 'prod':
        number_of_calls_status21 = NumberOfCalls.number_of_calls_status21_prod
    else:
        number_of_calls_status21 = NumberOfCalls.number_of_calls_status21_dev
    yield number_of_calls_status21


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_number_of_calls_status22(request):
    env_number_of_calls_status22 = request.config.getoption('--env')

    if env_number_of_calls_status22 == 'prod':
        number_of_calls_status22 = NumberOfCalls.number_of_calls_status22_prod
    else:
        number_of_calls_status22 = NumberOfCalls.number_of_calls_status22_dev
    yield number_of_calls_status22


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_number_of_calls_status23(request):
    env_number_of_calls_status23 = request.config.getoption('--env')

    if env_number_of_calls_status23 == 'prod':
        number_of_calls_status23 = NumberOfCalls.number_of_calls_status23_prod
    else:
        number_of_calls_status23 = NumberOfCalls.number_of_calls_status23_dev
    yield number_of_calls_status23


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_get_custom_card_call(request):
    env_get_custom_card_call = request.config.getoption('--env')

    if env_get_custom_card_call == 'prod':
        card_id_call = CardId.card_id_172
    else:
        card_id_call = CardId.card_id_13
    yield card_id_call


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_get_custom_card_chat(request):
    env_get_custom_card_chat = request.config.getoption('--env')

    if env_get_custom_card_chat == 'prod':
        card_id_chat = CardId.card_id_131
    else:
        card_id_chat = CardId.card_id_9
    yield card_id_chat


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_delete_card_id(request):
    env_delete_card_id = request.config.getoption('--env')

    if env_delete_card_id == 'prod':
        delete_card_id = CardId.card_id_delete_prod
    else:
        delete_card_id = CardId.card_id_delete_dev
    yield delete_card_id


# создаем фикстуру create_new_call_task_bulk, данные из которой будем использовать в тестах
# в тесты передаем переменные res_call_json, rand_pass1, rand_pass2
@pytest.fixture(scope="class")
def create_new_call_task_bulk(env_url_create_bulk, env_api_key, env_project_id, env_tag_5):

    # rand_pass - это переменные, по которым я проверяю что в ответе прищел именно тот звонок, который мне нужен
    rand_pass1 = random.randint(1, 10000)
    rand_pass2 = random.randint(1, 10000)

    # готовим данные для звонка
    data_other1 = json.dumps({'number': rand_pass1})
    data_other2 = json.dumps({'number': rand_pass2})
    data_for_call = json.dumps([
        {'phone': PhoneNumber.phone1, 'data': data_other1},
        {'phone': PhoneNumber.phone2, 'data': data_other2},
    ])

    # отправляем запрос на создание звонка и передаем параметры в тесты
    data_call = {'api_key': env_api_key, 'project_id': env_project_id, 'tag': env_tag_5,
                 'data': data_for_call}
    res_call_json = requests.post(env_url_create_bulk, data=data_call).json()
    # return {"res_call_json": res_call_json, "rand_pass1": rand_pass1, "rand_pass2": rand_pass2}
    yield res_call_json, rand_pass1, rand_pass2


# создаем фикстуру create_new_call_task, данные из которой будем использовать в тестах
# в тесты передаем переменные res_call_json, rand_pass1, rand_pass2
@pytest.fixture
def create_new_call_task(env_url_create, env_api_key, env_project_id):

    # rand_pass - это переменные, по которым я проверяю что в ответе прищел именно тот звонок, который мне нужен
    rand_pass1 = random.randint(1, 10000)

    # готовим данные для звонка
    data_other1 = json.dumps({'number': rand_pass1})
    # data_for_call = json.dumps([
    #     {'data': data_other1},
    #     ])

    # отправляем запрос на создание звонка и передаем параметры в тесты
    data_call = {'api_key': env_api_key, 'project_id': env_project_id, 'phone': PhoneNumber.phone1,
                 'data': data_other1}
    res_call_json = requests.post(env_url_create, data=data_call).json()
    yield res_call_json, rand_pass1
