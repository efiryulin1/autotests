
# Данные о переменных, разделены на классы. Используется в UI-тестах

import random
from datetime import datetime


class Variables:
    url_dev = "https://godev.robotmia.ru/login"
    url_prod = "https://go.robotmia.ru/login"
    username_client = 'user_client'
    username_scenario = "user_scenario"
    password = "A!B@C#D$E%"
    title = 'MIA | Login'
    my_projects = 'Проекты'
    runs = 'Исходящие запуски'
    new_run = 'Новый запуск'
    date_today = datetime.now()
    run_name_xlsx = f"Запуск xlsx {date_today}"
    run_name_xls = f"Запуск xls {date_today}"
    run_name_scv = f"Запуск scv {date_today}"
    run_name_ods = f"Запуск ods {date_today}"
    run_name_170k = f"Запуск 170k {date_today}"
    run_name = ''
    project_name = "Для автотестов исходящих по таблице"
    file_path_xlsx = "F:/autotests/helpers/Для автотеста.xlsx"
    file_path_xls = "F:/autotests/helpers/Для автотеста.xls"
    file_path_csv = "F:/autotests/helpers/Для автотеста.csv"
    file_path_ods = "F:/autotests/helpers/Для автотеста.ods"
    file_path_170k = "F:/autotests/helpers/Для автотеста (170k).xlsx"

    # file_path_xlsx = "/home/evgeniy/Для автотеста.xlsx"
    # file_path_xls = "/home/evgeniy/Для автотеста.xls"
    # file_path_csv = "/home/evgeniy/Для автотеста.csv"
    # file_path_ods = "/home/evgeniy/Для автотеста.ods"
    # file_path_170k = "/home/evgeniy/Для автотеста (170k).xlsx"
    rand_audio = random.randint(0, 1499)
    audio_file_path = f"/home/evgeniy/audio/audio{rand_audio}.wav"
    audio_file = f"audio{rand_audio}.wav"
    # file_path = random.randint(0, 1499)
    run_status_wait = 'Ожидает запуска\n(отменить)'
    run_status_cancel = 'Запуск отменён'
    time_wait = 120
    call_speed = 2
    phone1 = "79231265383"
    phone2 = "79231265336"
    status = "Окончен (автоответчик)"
