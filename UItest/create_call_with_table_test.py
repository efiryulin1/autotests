
# тест на проверку создания звонка по таблицам.

import pytest
import time
from locators import Locators
from variables import Variables
from helpers import login, create_run
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


# данные для параметризации теста.
data_run = [Variables.run_name_xlsx, Variables.file_path_xlsx]

# параметры для логина в кабинет
data_login = [Variables.username_client]


@pytest.mark.positive
@pytest.mark.ui
@pytest.mark.smoke
# @pytest.mark.parametrize('data_run', data_run)
# @pytest.mark.parametrize('data_login', data_login)
@pytest.mark.usefixtures('driver')
@pytest.mark.usefixtures('env_url')
@pytest.mark.usefixtures('env_project')
def test_create_call_with_table(driver, env_url, env_project):

    # тут используется функция логина в кабинет login из helpers.py
    username = data_login
    login(driver, env_url, username)

    # тут используется функция загрузки файла create_run из helpers.py
    run_name = data_run[0]
    file_path = data_run[1]
    create_run(driver, run_name, file_path, env_project)

    driver.find_element(*Locators.analytics).click()  # идем в аналитику
    # time.sleep(30)
    driver.find_element(*Locators.analytics_call).click()  # еще раз идем в аналитику
    driver.find_element(*Locators.choose_runs).click()  # кликаем по окну выбора запуска
    driver.find_element(*Locators.search_field).click()  # кликаем по полю поиска запуска
    driver.find_element(*Locators.search_field).send_keys(run_name)  # ищем запуск
    driver.find_element(*Locators.choose_run).click()  # выбираем нужный запуск

    # пролистываем вниз страницы, до видимости элемента previous_page
    driver.find_element(*Locators.tag_body).send_keys(Keys.END)
    WebDriverWait(driver, Variables.time_wait).until(EC.visibility_of_element_located(Locators.previous_page))

    # перезагружаем страницу, до тех пор, пока не станет виден второй звонок в таблице
    for i in range(60):
        driver.find_element(*Locators.reload_table).click()
        time.sleep(10)
        if len(driver.find_elements(*Locators.table_row_two)) > 0:
            break

    # проверяем звонки в таблице (по номеру телефона и статусу)
    assert driver.find_element(*Locators.first_table_row_phone).text == Variables.phone2, "wrong phone"
    assert driver.find_element(*Locators.first_table_row_status).text == Variables.status, "wrong status"
    assert driver.find_element(*Locators.second_table_row_phone).text == Variables.phone1, "wrong phone"
    assert driver.find_element(*Locators.second_table_row_status).text == Variables.status, "wrong status"
