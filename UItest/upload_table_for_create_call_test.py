
# тест загрузки таблиц для звонков

import pytest
from variables import Variables
from locators import Locators
from helpers import login, create_run


# данные для параметризации теста: используются файлы 4 видов: xls, xlsx. ods, csv
data_run = [
    [Variables.run_name_xlsx, Variables.file_path_xlsx],
    [Variables.run_name_xls, Variables.file_path_xls],
    [Variables.run_name_scv, Variables.file_path_csv],
    [Variables.run_name_ods, Variables.file_path_ods],
]

# параметры для логина в кабинет
data_login = [Variables.username_client]


# тест загружает 4 вида таблиц
@pytest.mark.positive
@pytest.mark.ui
@pytest.mark.parametrize('data_login', data_login)
@pytest.mark.parametrize('data_run', data_run)
@pytest.mark.usefixtures('driver')
@pytest.mark.usefixtures('env_url')
def test_upload_table(driver, env_url, data_run, data_login, env_project):

    # тут используется функция логина в кабинет login из helpers.py
    username = data_login
    login(driver, env_url, username)

    # тут используется функция загрузки файла create_run из helpers.py
    run_name = data_run[0]
    file_path = data_run[1]
    # run_name_check = data_run[2]
    create_run(driver, run_name, file_path, env_project)

    # проверяем, что новый запуск создан и он в статусе ожидает запуска
    assert driver.find_element(*Locators.run_status_wait).text == Variables.run_status_wait
    driver.find_element(*Locators.stop_run).click()  # тормозим запуск (тест звонков в дургом тесте)

    # проверяем, что у нового запуска нужное название.
    assert driver.find_element(*Locators.run_name_check).text == run_name
