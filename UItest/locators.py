
# Данные о локаторах, разделены на классы. Используется в UI-тестах

from selenium.webdriver.common.by import By
from variables import Variables


class Locators:
    login_field = (By.XPATH, "//input[@id='login']")
    password_field = (By.XPATH, "//input[@id='password']")
    button_login = (By.XPATH, "//button[@type='submit']")
    project_prod = (By.XPATH, "//small[@class='float-left text-muted'][contains(text(), 'ID: 171')]")
    project_dev = (By.XPATH, "//small[@class='float-left text-muted'][contains(text(), 'ID: 14')]")
    # project = (By.LINK_TEXT, "Для автотестов исходящих по таблице")
    breadcrumbs_check = (By.CSS_SELECTOR, ".breadcrumb-item.active")
    runs = (By.LINK_TEXT, "Исходящие запуски")
    audio = (By.LINK_TEXT, "Аудио")
    create_new_run = (By.LINK_TEXT, "Создать запуск")
    run_name = (By.XPATH, "//input[@placeholder='Введите название']")
    run_name_check = (By.XPATH, "//td[@class='footable-visible'][contains(text(),'Запуск')]")
    # run_name_check_xlsx = (By.XPATH, "//td[@class='footable-visible'][contains(text(),'xlsx')]")
    # run_name_check_xls = (By.XPATH, "//td[@class='footable-visible'][contains(text(),'xls')]")
    # run_name_check_csv = (By.XPATH, "//td[@class='footable-visible'][contains(text(),'csv')]")
    # run_name_check_ods = (By.XPATH, "//td[@class='footable-visible'][contains(text(),'ods')]")
    upload_audio = (By.ID, 'send_files')
    input_file = (By.XPATH, "//input[@type='file']")
    choose_column = (By.XPATH, "//option[@value='4']")
    call_speed = (By.XPATH, "//input[@type='number']")
    create_run = (By.CSS_SELECTOR, ".float-right.btn-save")
    run_status_wait = (By.XPATH, "//td[@class = 'footable-visible'][h5 = 'Ожидает запуска']")
    run_status_cancel = (By.XPATH, "//td[@class = 'footable-visible'][h5 = 'Запуск отменён']")
    stop_run = (By.XPATH, "//a[@class = 'text-success'][contains(text(),'(отменить)')]")
    analytics = (By.XPATH, "//a[@class='nav-link dropdown-toggle'][contains(text(),'Аналитика')]")
    analytics_call = (By.XPATH, "//a[@class='nav-link dropdown-item'][contains(text(),'звонков')]")
    choose_runs = (By.CSS_SELECTOR, ".select2-selection__rendered")
    search_field = (By.CSS_SELECTOR, ".select2-search__field")
    choose_run = (By.XPATH, "//li[@class='select2-results__option select2-results__option--highlighted']")
    tag_body = (By.TAG_NAME, "body")
    previous_page = (By.CSS_SELECTOR, ".paginate_button.page-item.previous.disabled")
    reload_table = (By.XPATH, "//span[contains(text(),'Перезагрузить')]")
    table_row_one = (By.XPATH, "//tr[@class = 'odd']")
    table_row_two = (By.XPATH, "//tr[@class = 'even']")
    first_table_row_phone = (By.XPATH, "//tr[@class='odd']/td[2]")
    first_table_row_status = (By.XPATH, "//tr[@class='odd']/td[9]")
    second_table_row_phone = (By.XPATH, "//tr[@class='even'][1]/td[2]")
    second_table_row_status = (By.XPATH, "//tr[@class='even'][1]/td[9]")
    first_table_row_audio = (By.XPATH, "//tr[@class='odd']/td[@class='sorting_1']")
    second_table_row_audio = (By.XPATH, "//tr[@class='even']/td[@class='sorting_1']")
    delete_audio = (By.ID, "delete_file")


