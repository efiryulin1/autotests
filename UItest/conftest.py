
# файл содержит фикстуры, которые запускаются до запуска теста (при необходимости)

import pytest
from selenium import webdriver
from variables import Variables
from locators import Locators
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


# функция принимает аргумент ('prod' или 'dev') из командной строки.
# От него зависит окружение, на котором запускается тест
def pytest_addoption(parser):
    parser.addoption(
        '--env', action='store', default='dev', help='available environments: dev, prod'
    )


# определяем окружение для запуска теста, если 'prod' то идем на go.robotmia.ru, в остальных случаях godev.robotmia.ru
@pytest.fixture
def env_url(request):
    env_url = request.config.getoption('--env')

    if env_url == 'prod':
        url = Variables.url_prod
    else:
        url = Variables.url_dev
    yield url


# определяем окружение для запуска теста, если 'prod' то идем на в 171 проект, в остальных случаях 14 проект
@pytest.fixture
def env_project(request):
    env_project = request.config.getoption('--env')

    if env_project == 'prod':
        project = Locators.project_prod
    else:
        project = Locators.project_dev
    yield project


# фикстура инициализации веб-драйвера, используется во всех тестах
@pytest.fixture
def driver():

    # прописываем параметры веб-драйвера
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")  # запустить тест без открытия окон браузера
    options.add_argument("--start-maximized")  # запустить тест с окном на весь экран

    driver = webdriver.Chrome(options=options)

    # driver = webdriver.Remote(
    #     command_executor='http://127.0.0.1:4444/wd/hub',
    #     options=options
    # )

    # driver = webdriver.Chrome('/home/evgeniy/Downloads/chromedriver_linux64/chromedriver', options=options)  # для ноута
    yield driver
    driver.close()
