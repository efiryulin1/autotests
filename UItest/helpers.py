import time

# файл со вспомогательными функцими, которые используются в разных UI-тестах (вроде лоигна)

import pytest
from locators import Locators
from variables import Variables
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# функция логина в кабинет
def login(driver, url, username):

    driver.get(url)  # переходим по ссылке (переменные в отдельном файле variables.py)

    # устанавливаем параметр cookie_accept = true, чтобы убрать плашку про условия пользования сайтом
    driver.execute_script("window.localStorage.setItem('cookie_accept', 'true')")

    # находим на странице поле для ввода логина (локаторы в отдельном файле locators.py) и вводим значение из переменной
    driver.find_element(*Locators.login_field).send_keys(username)

    # находим поле для ввода пароля и вводим значение
    driver.find_element(*Locators.password_field).send_keys(Variables.password)
    driver.find_element(*Locators.button_login).click()  # кликаем по кнопке логина


# функция загрузки файлов в исходящие запуски

def create_run(driver, run_name, file_path, env_project):
    driver.find_element(*env_project).click()  # идем в проект
    driver.find_element(*Locators.runs).click()  # идем в исходящие запуски
    driver.find_element(*Locators.create_new_run).click()  # кликаем по кнопке создания запуска
    driver.find_element(*Locators.run_name).send_keys(run_name)  # вводим название запуска

    # загружаем файл для обзвона и ждем пока появится таблица с выбором колонок
    driver.find_element(*Locators.input_file).send_keys(file_path)
    WebDriverWait(driver, Variables.time_wait).until(EC.presence_of_element_located(Locators.choose_column))

    driver.find_element(*Locators.choose_column).click()  # выбираем колонку с номером телефона
    driver.find_element(*Locators.call_speed).clear()  # удаляем дефолтную скорость обзвона
    driver.find_element(*Locators.call_speed).send_keys(Variables.call_speed)  # ставим скорость звонков в минуту

    # создаем новый запуск и ждем пока откроется страница с созданием нового запуска
    driver.find_element(*Locators.create_run).click()
    WebDriverWait(driver, Variables.time_wait).until(EC.presence_of_element_located(Locators.create_new_run))



