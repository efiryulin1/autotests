
# тест загрузки аудио на проект

import pytest
from locators import Locators
from variables import Variables
from helpers import login
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# параметры для лоигна в кабинет
data_login = [Variables.username_scenario]


# тест загрузки аудион на проект
@pytest.mark.positive
@pytest.mark.ui
@pytest.mark.parametrize('data_login', data_login)
@pytest.mark.usefixtures('driver')
@pytest.mark.usefixtures('env_url')
def test_upload_audio(driver, env_url, data_login, env_project):

    # берем параметры для логина и используем функцию login из helpers.py
    username = data_login
    login(driver, env_url, username)

    driver.find_element(*env_project).click()  # идем в 14 проект
    driver.find_element(*Locators.audio).click()  # идем в аудио
    driver.find_element(*Locators.input_file).send_keys(Variables.audio_file_path)  # загружаем аудио
    driver.find_element(*Locators.upload_audio).click()  # кликаем по кнопке загрузке

    # ждем пока аудио загрузиться
    WebDriverWait(driver, Variables.time_wait).until(EC.presence_of_element_located(Locators.first_table_row_audio))

    # проверяем,что загружен только один файл и проверяем его название
    assert len(driver.find_elements(*Locators.first_table_row_audio)) == 1
    assert len(driver.find_elements(*Locators.second_table_row_audio)) == 0
    assert driver.find_element(*Locators.first_table_row_audio).text == Variables.audio_file

    # удалаем загруженный файл
    driver.find_element(*Locators.delete_audio).click()
    driver.switch_to.alert.accept()


# @pytest.mark.positive
# @pytest.mark.ui
# @pytest.mark.parametrize('data_login', data_login)
# @pytest.mark.usefixtures('driver')
# def test_upload_several_audio(driver, data_login):
#     username = data_login
#     login(driver, username)
#
#     driver.find_element(*Locators.project).click()  # идем в 14 проект
#     driver.find_element(*Locators.audio).click()
#     for i in range(10):
#         # driver.find_element(*Locators.input_file).send_keys(Variables.audio_file_path)
#         # driver.find_element(*Locators.input_file).send_keys(audio_file_path)
#         # Variables.rand_audio += 1
#         rand_audio = random.randint(0, 1499)
#         audio_file_path = f"/home/evgeniy/audio/audio{rand_audio}.wav"
#         driver.find_element(*Locators.input_file).send_keys(audio_file_path)
#     driver.find_element(*Locators.upload_audio).click()
#     WebDriverWait(driver, Variables.time_wait).until(EC.presence_of_element_located(Locators.first_table_row_audio))
#
#     assert len(driver.find_elements(*Locators.first_table_row_audio)) == 1
#     assert len(driver.find_elements(*Locators.second_table_row_audio)) == 1
#     # assert len(driver.find_elements(*Locators.first_table_row_audio)) == 5
#     # assert len(driver.find_elements(*Locators.second_table_row_audio)) == 5
#     # for i in range(10):
#     #     # assert driver.find_element(*Locators.first_table_row_audio).text == Variables.audio_file
#     #     # assert driver.find_element(*Locators.second_table_row_audio).text == Variables.audio_file
#     #
#     for i in range(2):
#         driver.find_element(*Locators.delete_audio).click()
#         # time.sleep(1)
#         driver.switch_to.alert.accept()
#         # WebDriverWait(driver, Variables.time_wait).until(EC.presence_of_element_located(Locators.delete_audio))
#         # time.sleep(1)
#         WebDriverWait(driver, Variables.time_wait).until(EC.element_to_be_selected(Locators.delete_audio))
