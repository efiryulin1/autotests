
# Тест логина в кабинет

import pytest
from locators import Locators
from variables import Variables
from helpers import login

# данные для логина
data_login = [Variables.username_client], [Variables.username_scenario]


#  тестовая функция использвует фикстуру driver и  login (из conftest.py), поэтому сам тест состоит только из проверки
@pytest.mark.positive
@pytest.mark.ui
@pytest.mark.parametrize('data_login', data_login)
@pytest.mark.usefixtures('driver')
@pytest.mark.usefixtures('env_url')
def test_login(driver, env_url, data_login):
    username = data_login
    login(driver, env_url, username)

    # проверяем, что мы залогинились
    assert driver.find_element(*Locators.breadcrumbs_check).text == Variables.my_projects
