import os
import time
import telebot
from telebot import types


token = '6091816727:AAGfhx-UwwesNfOdKi-JQl_yzgQyNa4TJyM'
bot = telebot.TeleBot(token)
path = "logs/logs.txt"
pathUI = "logs/logsUI.txt"


@bot.message_handler(commands=["start"])  # Обработка /start
def handle_start(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    get_result_button_api = types.KeyboardButton("/result_API")
    get_result_button_ui = types.KeyboardButton("/result_UI")
    run_test_button_api = types.KeyboardButton("/run_tests_API")
    run_test_button_ui = types.KeyboardButton("/run_tests_UI")
    markup.row(get_result_button_api)
    markup.row(run_test_button_api)
    markup.row(get_result_button_ui)
    markup.row(run_test_button_ui)
    bot.send_message(message.chat.id, 'Чтобы получить результат прохождения теста нажми на кнопку /result', reply_markup=markup)
    bot.send_message(message.chat.id, 'Чтобы запустить проверку нажми на кнопку /run_tests', reply_markup=markup)


@bot.message_handler(commands=["result_API"])  # Обработка /result
def handle_result_api(message):
    with open(path, "r", encoding='utf-8') as file:
        data = file.read()
    mod_time_second = (os.stat(path).st_mtime + 25200)
    mod_time = time.strftime("%d.%m %H:%M:%S", time.gmtime(mod_time_second))
    bot.send_message(message.chat.id, data)
    bot.send_message(message.chat.id, f"Время обновления файла с логом API-тестов {mod_time}")


@bot.message_handler(commands=["run_tests_API"])  # Обработка /run_tests
def handle_run_test_api(message):
    bot.send_message(message.chat.id, "Запущено выполнение тестов. Время выполнения до 10 минут")
    os.system('cd apitest/ && pytest -svm smoke --env prod --tb=line >../logs/logs.txt')
    # os.system('. venv/bin/activate && cd apitest/ && pytest -svm smoke --env prod --tb=line >../logs/logs.txt')
    bot.send_message(message.chat.id, "Тесты завершены. Нажми /result_API, чтобы посомтреть лог")


@bot.message_handler(commands=["result_UI"])
def handle_result_ui(message):
    with open(pathUI, "r", encoding='utf-8') as file:
        data = file.read()
    mod_time_second = (os.stat(pathUI).st_mtime + 25200)
    mod_time = time.strftime("%d.%m %H:%M:%S", time.gmtime(mod_time_second))
    bot.send_message(message.chat.id, data)
    bot.send_message(message.chat.id, f"Время обновления файла с логом UI-тестов {mod_time}")


@bot.message_handler(commands=["run_tests_UI"])
def handle_run_test_ui(message):
    bot.send_message(message.chat.id, "Запущено выполнение тестов. Время выполнения до 2 минут")
    os.system('cd UItest/ && pytest -svm smoke --env prod --tb=line >../logs/logsUI.txt')
        # os.system('. venv/bin/activate && cd UItest/ && pytest -svm smoke --env prod --tb=line >../logs/logsUI.txt')
    bot.send_message(message.chat.id, "Тесты завершены. Нажми /result_UI, чтобы посомтреть лог")

# bot.polling(none_stop=True, interval=0)

try:
  bot.polling()
except Exception as e:
  print(e)
